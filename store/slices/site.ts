import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../index'

/**
 * Начальное состояние редьюсера Site/
 */
export const initialState = {
    /** Объект авторизованного пользователя. */
    sideMenuOpened: false as boolean,
}

// -------------------------------------------
// Slice
// -------------------------------------------

/**
 * Создание слайса Site.
 */
export const siteSlice = createSlice({
    name: 'site',
    initialState,
    reducers: {
        changeMenuStatus: (state, action: PayloadAction<boolean>) => { state.sideMenuOpened = action.payload }
    },
    //extraReducers: (builder) => {},
})

// -------------------------------------------
// Selectors
// -------------------------------------------

/**
 * Селектор статуса сайд бара.
 * @param state Объект стора
 */
export const selectSideMenuStatus = (state: RootState) => state.site.sideMenuOpened

// -------------------------------------------
// Export a reducer
// -------------------------------------------
export default siteSlice.reducer
