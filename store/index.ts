import { configureStore } from '@reduxjs/toolkit'
import siteReducer from './slices/site'

export const store = configureStore({
    reducer: {
        site: siteReducer,
    },
})

// Вывод типов `RootState` и `AppDispatch` из стора.
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
