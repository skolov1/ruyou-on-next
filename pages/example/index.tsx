import styles from './Example.module.scss'
import { FC, useState } from 'react'
import ExampleSVG from '../../public/example-black.svg'
// import SVGMorpheus from 'svg-morpheus'

type Props = {}

const Example: FC<Props> = () => {

    // const myIcons = new SVGMorpheus('#myIconSet');


    return (
        <div className={styles.wrapper}>
            <ExampleSVG />
        </div>
    )
}
export default Example