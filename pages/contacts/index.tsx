// import contactsPageStyle from './Contacts.module.scss'
import { Head } from '../../components/complexes/Head'
import { Header } from '../../components/complexes/Header'
import { Footer } from '../../components/complexes/Footer'
import { SideSocial } from '../../components/complexes/SideSocial'
import { OpenedCity } from '../../components/complexes/OpenedCity'
import { FC, useCallback, useState } from 'react'
import { motion } from 'framer-motion'
import { StylesContext } from '../_app'
import { useContext } from 'react'

type Props = {}


export type Cities = {
    id: number,
    city: string,
    address: string,
    mail: string,
    phone: string,
    mapCenter: {
        center: Array<number>,
        zoom: number,
    },
    placemark: Array<number>,
}

const CitiesIn: Cities[] = [
    {
        id: 1,
        city: 'Москва',
        address: 'Москва, Серебряническая набережная, 29',
        mail: 'support@ruyou.ru',
        phone: '+7 (499) 444-13-27',
        mapCenter: { center: [55.751990, 37.627637], zoom: 12.95 },
        placemark: [55.750504, 37.653191],
    },
    {
        id: 2,
        city: 'Калининград',
        address: 'г. Калининград, ул. Уральская, д. 18, 4 этаж, 4 офис.',
        mail: 'support@ruyou.ru',
        phone: '+7 (343) 243-55-02',
        mapCenter: { center: [54.711716, 20.516159], zoom: 12.95 },
        placemark: [54.724623, 20.502276],
    },
    {
        id: 3,
        city: 'Екатеринбург',
        address: 'г. Екатеринбург, ул. Библиотечная 25',
        mail: 'support@ruyou.ru',
        phone: '+7 (343) 243-55-02',
        mapCenter: { center: [56.833017, 60.613108], zoom: 12.55 },
        placemark: [56.833801, 60.649649],
    },
]

const Contacts: FC<Props> = () => {

    const [openedCity, setOpenedCity] = useState<number>()
    const { contactsPageStyle } = useContext(StylesContext)
    const onHandleClose = useCallback(() => {
        setOpenedCity(undefined)
    }, [])

    return (
        <motion.div
            className={contactsPageStyle.container}
            animate={{ opacity: 1 }}
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
        >

            <Head />

            <main className={contactsPageStyle.main}>
                <Header />

                <section className={contactsPageStyle.wrapper}>

                    <div className={contactsPageStyle.contactsHolder}>
                        <div className={contactsPageStyle.headerTitle}>Контакты</div>
                        <div className={contactsPageStyle.contactsItems}>
                            {CitiesIn.map(c =>
                                <div
                                    key={c.id}
                                    className={contactsPageStyle.contactsItem}
                                >
                                    <div
                                        onClick={() => setOpenedCity(c.id)}
                                        className={contactsPageStyle.contactsCity}
                                    >{c.city}:</div>
                                    <div className={contactsPageStyle.contactsAddress}>{c.address}</div>
                                    <div className={contactsPageStyle.contactsMail}>{c.mail}</div>
                                    <div className={contactsPageStyle.contactsPhone}>{c.phone}</div>
                                </div>
                            )}
                        </div>
                    </div>

                    <SideSocial />

                </section>

                <Footer minimal />

                {
                    openedCity &&
                    <OpenedCity
                        onClose={onHandleClose}
                        city={CitiesIn.find(c => c.id === openedCity)}
                    />
                }

            </main>

        </motion.div>
    )
}
export default Contacts