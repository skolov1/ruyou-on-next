import someMetroStyles from './someMetroStyles.module.scss'
// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Metro from '../../../public/projects/logo/metro-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/metro/metro-pic-1.png',
    '/projects/metro/metro-pic-2.png',
    '/projects/metro/metro-pic-3.png',
    '/projects/metro/metro-pic-4.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)


    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект Metro | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/metro/metro-proj.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2020</div>*/}
                            <Metro className={projectPlatePageStyle.projectLogo} />
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>
                        <div className={projectPlatePageStyle.mainWrapCentered}>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Оптимизация затрат на работу промоутеров, сбор информации о продвигаемых продуктах,
                                    информирование клиентов о продукции, возможность легко и оперативно получать от клиентов обратную
                                    связь. Проще говоря, компании нужен был промоутер, который может привлечь покупателя, провести
                                    дегустацию, узнать о его впечатлениях и обработать эту информацию.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>440</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>1 разработчик</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>


                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={classNames([projectPlatePageStyle.paragraph, projectPlatePageStyle.paragraphGotham])}>
                                    Онлайн-промоутер, который умеет:
                                </p>

                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Экономить рекламный бюджет </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Проводить опросы и получать обратную связь </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Собирать статистику и аналитику по промо-акциям </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Привлекать потенциальных покупателей к дегустации продукта </li>
                                </ul>

                                <p className={projectPlatePageStyle.paragraph} style={{ marginTop: 20 }}>
                                    Для реализации этой идеи нам понадобилась стойка с продукцией и интерактивный экран. Добавили к
                                    нему аудиосопровождение, чтобы покупатель обратил внимание на дегустацию, а в итоге совершил
                                    целевое действие - покупку. Покупатель оценивает вкусовые или иные свойства продукта,
                                    отвечает на вопросы на интерактивном экране, а за это получает определенный бонус.
                                    Приложение собирает все такие отзывы и в обработанном виде предоставляет данные
                                    админимтрации магазина и производителю товара, а так же показывает рейтинг товара
                                    другим потенциальным покупателям.
                                </p>
                            </div>





                            {/* <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.imageProjectHolder}>
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-1.png" alt="" />
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-2.png" alt="" />
                                    </div>
                                </div>
                            </ScrollAnimation> */}

                            {/* <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointSecondTitle}>
                                    Pixli успешно экономит драгоценное время разработчиков:
                                </div>
                                <ul className={projectPlatePageStyle.projectList}>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Многопользовательский доступ - изменения в проект могут вносить одновременно несколько разработчиков
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Копирование между проектами - для однотипных проектов повторяющиеся элементы можно просто скопировать
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Функция White Label - передача сайта заказчику через отдельный личный кабинет
                                    </li>
                                </ul>
                            </div> */}

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slides} />
                                </div>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                    <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                    <div className={projectPlatePageStyle.colorsWrap}>

                                        <div className={projectPlatePageStyle.sideWrap}>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#111111' }} />
                                                Primary Text
                                            </div>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#5A5A5A' }} />
                                                Secondary Text
                                            </div>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#A3A3A3' }} />
                                                Tertiary Text
                                            </div>
                                        </div>

                                        <div className={projectPlatePageStyle.mainWrap}>
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#00387F' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#1256AC' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#417FCC' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#6696D2' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#99B7DD' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#226C99' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#3189BE' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#52A4D6' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#98C8E4' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C5DDEB' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFE300' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFE939' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFEE61' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFF285' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFF7B3' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D95E3F' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FC7B5A' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FF957A' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFB6A4' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFD9CF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#45A163' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#79D096' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#B0FFCB' }} />
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                    <div className={projectPlatePageStyle.leftSide}>
                                        <div className={someMetroStyles.fontHeader}>Montserrat</div>
                                        <div className={someMetroStyles.fontUpperCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                        <div className={someMetroStyles.fontLowerCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                        <div className={someMetroStyles.fontNumber}>1  2  3  4  5  6  7  8  9  0</div>
                                    </div>

                                    <div className={projectPlatePageStyle.rightSide}>
                                        <div className={someMetroStyles.fontHdr}>Header</div>
                                        <div className={someMetroStyles.fontTitle}>Title</div>
                                        <div className={someMetroStyles.fontSubtitle}>Subtitle</div>

                                        <div className={someMetroStyles.bodyOne}>
                                            <span style={{paddingRight: '91px'}}>Body 1</span>
                                            <span style={{fontWeight: 500}}>Body 1 - Medium</span>
                                        </div>
                                        <div className={someMetroStyles.bodyTwo}>
                                            <span style={{paddingRight: '94px'}}>Body 2</span>
                                            <span style={{fontWeight: 500}}>Body 2 - Medium</span>
                                        </div>
                                        <div className={someMetroStyles.button}>
                                            <span>Button</span>
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.projectDevider} />

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/ural2019-2021proj.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    Урал*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Другие проекты
                                    </div>
                                    <Link href="/projects/9">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проекты
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                            <div className={projectPlatePageStyle.mouseHolder}>
                                <MouseIcon />
                            </div>

                        </div>

                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts