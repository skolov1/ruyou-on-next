// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import someSmallStyles from './someSmallStyles.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Metro from '../../../public/projects/logo/small-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/small/small-pic-2.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект Small | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/small/small-proj.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2020</div>*/}
                            <Metro className={projectPlatePageStyle.projectLogo} />
                            <p className={projectPlatePageStyle.projectDescription}>
                                Мобильное приложение для сети безкассовых супермаркетов
                            </p>
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                            <p className={projectPlatePageStyle.paragraph}>
                            Реализовать мобильное приложение для сети безкассовых магазинов Small,
                            с помощью которого клиенты магазина могут совершать покупки без участия продавцов
                            и кассиров. 
                            </p>
                            <p className={projectPlatePageStyle.paragraph}>
                            Теперь клиенту магазина достаточно отсканировать товары с помощью мобильного приложения и там же совершить оплату,
                            в том числе с помощью  Apple Pay и Google Pay.
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                <div className={projectPlatePageStyle.developParams}>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>740</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                    </DevelopCounter>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>4 разработчика</div>
                                        <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                    </DevelopCounter>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                <div className={projectPlatePageStyle.colorsWrap}>

                                    <div className={projectPlatePageStyle.sideWrap}>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#1A1D1E' }} />
                                            Primary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#7E7E82' }} />
                                            Secondary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#8D8C8C' }} />
                                            Tertiary Text
                                        </div>
                                    </div>

                                    <div className={projectPlatePageStyle.mainWrap}>
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#537DB7' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#6F9DDD' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#7CADF0' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#99BBEB' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C9E0FF' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F5CA44' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F8D66C' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F1D682' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFEAA9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F9E9B9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D24A44' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E46862' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#ED7B76' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F68A86' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E9B9B7' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#614F7D' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#836BA8' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#A089C5' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#AE94D7' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C2B2DB' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#35A248' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#50BD63' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#72ED87' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#98E6A6' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C7F9D0' }} />
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                <div
                                    className={classNames(
                                        projectPlatePageStyle.leftSide,
                                        projectPlatePageStyle.gilroyFont
                                    )}
                                >
                                    <div className={someSmallStyles.fontHeader}>Gilroy</div>
                                    <div className={someSmallStyles.fontUpperCase}>
                                        A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z
                                    </div>
                                    <div className={someSmallStyles.fontNumber}>
                                        1  2  3  4  5  6  7  8  9  0
                                    </div>
                                </div>

                                <div
                                    className={classNames(
                                        projectPlatePageStyle.rightSide,
                                        projectPlatePageStyle.gilroyFont
                                    )}
                                    style={{
                                        maxWidth: '50%',
                                        paddingTop: '118px'
                                    }}>
                                    <div className={someSmallStyles.fontLowerCase}>
                                        A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z
                                    </div>
                                    <div className={someSmallStyles.fontNumber}>
                                        1  2  3  4  5  6  7  8  9  0
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.itemHolderFullScreen)}>
                                <div className={projectPlatePageStyle.pointTitle}>Прототипы приложения</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Wireframes</p>
                            </div>
                            <img className={projectPlatePageStyle.imageForHuge} src="/projects/small/small-prototype.svg" />

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>
                                    Дизайн
                                </div>
                                <p className={projectPlatePageStyle.addPointTitle}>Design</p>
                                <div className={projectPlatePageStyle.mobileDesign}>
                                    <img src='/projects/small/small-design-1.png' />
                                    <img src='/projects/small/small-design-2.png' />
                                </div>
                            </div>
                        </ScrollAnimation>


                        {/* <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Онлайн-промоутер, который умеет:
                                </p>

                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Экономить рекламный бюджет </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Проводить опросы и получать обратную связь </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Собирать статистику и аналитику по промо-акциям </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Привлекать потенциальных покупателей к дегустации продукта </li>
                                </ul>

                                <p className={projectPlatePageStyle.paragraph} style={{ marginTop: 20 }}>
                                    Для реализации этой идеи нам понадобилась стойка с продукцией и интерактивный экран. Добавили к
                                    нему аудиосопровождение, чтобы покупатель обратил внимание на дегустацию, а в итоге совершил
                                    целевое действие - покупку. Покупатель оценивает вкусовые или иные свойства продукта,
                                    отвечает на вопросы на интерактивном экране, а за это получает определенный бонус.
                                    Приложение собирает все такие отзывы и в обработанном виде предоставляет данные
                                    админимтрации магазина и производителю товара, а так же показывает рейтинг товара
                                    другим потенциальным покупателям.
                                </p>
                            </div> */}





                        {/* <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.imageProjectHolder}>
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-1.png" alt="" />
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-2.png" alt="" />
                                    </div>
                                </div>
                            </ScrollAnimation> */}

                        {/* <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointSecondTitle}>
                                    Pixli успешно экономит драгоценное время разработчиков:
                                </div>
                                <ul className={projectPlatePageStyle.projectList}>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Многопользовательский доступ - изменения в проект могут вносить одновременно несколько разработчиков
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Копирование между проектами - для однотипных проектов повторяющиеся элементы можно просто скопировать
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Функция White Label - передача сайта заказчику через отдельный личный кабинет
                                    </li>
                                </ul>
                            </div> */}

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.functionalColumns}>
                                    <div>
                                        <p className={projectPlatePageStyle.functionalColumnsHeader}>
                                            Технологический стек
                                        </p>
                                        <p className={projectPlatePageStyle.functionalColumnsText}>
                                            Кроссплатформенное мобильное приложение для iOS и Android, интегрированное с 1С
                                        </p>
                                        <ul className={projectPlatePageStyle.functionalColumnsList}>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Backend - Python</li>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Frontend - React Native</li>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Интеграции - 1С, firebase </li>
                                        </ul>
                                    </div>
                                    <div>
                                        <p className={projectPlatePageStyle.functionalColumnsHeader}>
                                            Функционал:
                                        </p>
                                        <ul className={projectPlatePageStyle.functionalColumnsList}>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Регистрация и авторизация</li>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Возможность сканирования штрих-кода товара с помощью встроенного сканера</li>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Добавление товаров в корзину</li>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Оплата покупок, в том числе с помощью Apple Pay и Google Pay</li>
                                            <li className={projectPlatePageStyle.functionalColumnsItem}>Бонусная программа</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <ProjectImageSlider slides={slides} />
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.projectDevider} />
                        </div>


                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/premier/premier-background.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    Готовится к релизу 2021*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Премьер Зал
                                    </div>
                                    <Link href="/projects/2">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.mouseHolder}>
                            <MouseIcon />
                        </div>


                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts