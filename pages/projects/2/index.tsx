// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import somePremierStyles from './somePremierStyles.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Premier from '../../../public/projects/logo/premier-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/premier/premier-pic-1.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект Премьер зал | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/premier/premier-background.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2021</div>*/}
                            <Premier className={projectPlatePageStyle.projectLogo} />
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                            <p className={projectPlatePageStyle.paragraph} style={{maxWidth:'890px'}}>
                            Мобильное приложение “Премьер зал” - приложение созданное для посетителей более 280 кинотеатров и дает им возможность 
                            совершать покупки билетов и продуктов бара в своем смартфоне. Приложение оснащено чат-ботом для регистрации, авторизации, отзывов.
                            Также за совершенные покупки и походы в кино пользователь получает баллы, которыми после может расплачиваться в сети кинотеатров.
                            В личном кабинете пользователь может выставить свои интересы: любимый жанр кино и любимый попкорн. Покупать билеты и продукцию бара заранее.
                            Общаться с единомышленниками, создавать группы по интересам, получать дополнительные бонсы, скидки и награды за активность.
                            В личном кабинете пользователь приложения может посмотреть историю своих заказов, как билетов, так и бара и по желанию повторить их. 
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                <div className={projectPlatePageStyle.developParams}>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>790</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                    </DevelopCounter>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                        <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                    </DevelopCounter>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>
                                    Дизайн
                                </div>
                                <p className={projectPlatePageStyle.addPointTitle}>Design</p>
                                <div className={projectPlatePageStyle.mobileDesign}>
                                    <img src='/projects/premier/premier-design-1.png' />
                                    <img src='/projects/premier/premier-design-2.png' />
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>
                                Результат
                            </div>
                            <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 25px 0',fontFamily:'Gotham Pro',fontWeight:'normal'}}>Не просто мобильное приложение,
                            а целая экосистема</p>
                            <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 6px 0',fontFamily:'Gotham Pro',fontWeight:500,color:"white"}}>Функционал</p>
                            <p className={projectPlatePageStyle.paragraph} style={{margin:'0 0 38px 0',fontFamily:'Lato',fontWeight:'normal'}}>
                            Просмотр, трейлеров, сеансов. Покупка билетов в кино и продуктов бара кинотеатра.
                            Отзывы к фильмам и кинотеатрам сети с помощью чат бота. 
                            Бонусная система, личный кабинет пользователя. 
                            Раздел мероприятий
                            </p>
                            <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 6px 0',fontFamily:'Gotham Pro',fontWeight:500,color:"white"}}>
                            Технологический стек</p>
                            <p className={projectPlatePageStyle.paragraph} style={{margin:0,fontFamily:'Lato',fontWeight:'normal'}}>
                            Swift, Kotlin
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <ProjectImageSlider slides={slides} />
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder} style={{marginBottom:'175px'}}>
                                <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                <div className={projectPlatePageStyle.colorsWrap}>

                                    <div className={projectPlatePageStyle.sideWrap}>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#3B3B3B' }} />
                                            Primary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#5A5A5A' }} />
                                            Secondary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#D7D7D7' }} />
                                            Tertiary Text
                                        </div>
                                    </div>

                                    <div className={projectPlatePageStyle.mainWrap}>
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F39200' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FBB54B' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F4BA62' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F6CA88' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFE6C1' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#951B81' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#B935A3' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#CC54B8' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E083D1' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFBCF4' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F83600' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FA5D32' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FD8462' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFB29C' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFD0C2' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#0076E3' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#3293ED' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#5DAAF0' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#94C4F0' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#CDE7FF' }} />
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                <div className={projectPlatePageStyle.leftSide} style={{maxWidth:'580px'}}>
                                    <div className={somePremierStyles.fontHeader}>Noto Sans</div>
                                    <div className={somePremierStyles.fontUpperCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={somePremierStyles.fontLowerCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={somePremierStyles.fontNumber}>1  2  3  4  5  6  7  8  9  0</div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.projectDevider} />
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/bigam/bigam-pic-1.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    2020*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Bigam
                                    </div>
                                    <Link href="/projects/3">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.mouseHolder}>
                            <MouseIcon />
                        </div>


                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts