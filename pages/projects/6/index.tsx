// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Metro from '../../../public/projects/logo/suenco-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/suenco/suenco-pic-1.png',
    '/projects/suenco/suenco-pic-2.png',
    '/projects/suenco/suenco-pic-3.png',
    '/projects/suenco/suenco-pic-4.png',
    '/projects/suenco/suenco-pic-5.png',
    '/projects/suenco/suenco-pic-6.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект СУЭНКО | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/suenco/suenco-proj.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2021</div>*/}
                            <Metro className={projectPlatePageStyle.projectLogo} />
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>
                        <div className={projectPlatePageStyle.mainWrapCentered}>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Разработать сервис, как интранет-проект для управляющего звена компании, позволяющий более чем 2000 
                                    сотрудников оперативно доносить и получать информацию. Обеспечить гибкую настройку прав доступа к информации 
                                    и итоговым отчетам пользователей различного уровня.
                                </p>
                                <p className={projectPlatePageStyle.paragraph} style={{margin:0}}>
                                    WEB-сервис корпоративное приложение, для отображения статистической информации о компании. «Кабинет руководителя АО «СУЭНКО»
                                </p>
                                <p className={projectPlatePageStyle.paragraph} style={{margin:0}}> 
                                    Сервис решает несколько задач
                                </p>
                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Централизация данных</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Графическое отображение данных</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Возможность сравнительного анализа данных</li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>440</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>


                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 11px 0',fontFamily:'Gotham Pro',fontWeight:'normal'}}>Отчёты</p>
                                <p className={projectPlatePageStyle.paragraph} style={{margin:0}}>Каждый отчет имеет индивидуальную структуру и для загрузки отчета у пользователя 
                                есть несколько вариантов</p>
                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Заполнить форму отчета вручную</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Загрузить EXCEL файл</li>
                                </ul>
                                <p className={projectPlatePageStyle.paragraph}>После загрузки данных отчет сохраняется и раз в неделю передается руководителю автоматически.
                                Главный администратор в течение недели может видеть кто из каких сотрудников загрузил отчет и какие данные он предоставил.
                                </p>
                                <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 6px 0',fontFamily:'Gotham Pro',fontWeight:'normal'}}>Статистика</p>
                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>На основании отправленных данных собирается статистика, для этого мы 
                                    использовали библиотеку Apexcharts видоизменив ее под потребности клиента.</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>С помощью фильтра руководитель может выбрать одни из 25 типов отчетов</li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slides} />
                                </div>
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.functionalColumns}>
                                        <div>
                                            <p className={projectPlatePageStyle.functionalColumnsHeader} style={{marginBottom:'15px',fontFamily:'Gotham Pro',fontWeight:'normal'}}>
                                                Технологический стек
                                            </p>
                                            <p className={projectPlatePageStyle.paragraph} style={{margin:0}}> 
                                                База данных: PHP, MySQL
                                            </p>
                                            <p className={projectPlatePageStyle.paragraph} style={{margin:0}}> 
                                                WEB-сервис:
                                            </p>
                                            <p className={projectPlatePageStyle.paragraph} style={{margin:0}}> 
                                                Frontend: HTML, CSS, JavaScript
                                            </p>
                                        </div>
                                        <div>
                                            <p className={projectPlatePageStyle.functionalColumnsHeader} style={{marginBottom:'15px',fontFamily:'Gotham Pro',fontWeight:'normal'}}>
                                                Количество предполагаемых пользователей:
                                            </p>
                                            <p className={projectPlatePageStyle.paragraph} style={{margin:0}}> 
                                                2000 пользователей 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>




                            {/* <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.imageProjectHolder}>
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-1.png" alt="" />
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-2.png" alt="" />
                                    </div>
                                </div>
                            </ScrollAnimation> */}

                            {/* <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointSecondTitle}>
                                    Pixli успешно экономит драгоценное время разработчиков:
                                </div>
                                <ul className={projectPlatePageStyle.projectList}>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Многопользовательский доступ - изменения в проект могут вносить одновременно несколько разработчиков
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Копирование между проектами - для однотипных проектов повторяющиеся элементы можно просто скопировать
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Функция White Label - передача сайта заказчику через отдельный личный кабинет
                                    </li>
                                </ul>
                            </div> */}

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                    <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                    <div className={projectPlatePageStyle.colorsWrap}>

                                        <div className={projectPlatePageStyle.sideWrap}>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#111111' }} />
                                                Primary Text
                                            </div>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#5A5A5A' }} />
                                                Secondary Text
                                            </div>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#A3A3A3' }} />
                                                Tertiary Text
                                            </div>
                                        </div>

                                        <div className={projectPlatePageStyle.mainWrap}>
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#034EA2' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#2A55C4' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#607ECA' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#9B9BBC' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C7C7CF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#1A1A1A' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#3F3F3F' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#656565' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#B5B5B5' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C8C8C8' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E48900' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F05D1E' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#03A213' }} />
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                    <div
                                        className={classNames(
                                            projectPlatePageStyle.leftSide,
                                            projectPlatePageStyle.prostoFont
                                        )}
                                        style={{}}
                                    >
                                        <div className={projectPlatePageStyle.fontHeader}>Prosto One</div>
                                        <div className={projectPlatePageStyle.fontUpperCase}>
                                            A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z
                                        </div>
                                        <div className={projectPlatePageStyle.fontNumber}>
                                            1  2  3  4  5  6  7  8  9  0
                                        </div>
                                    </div>

                                    <div
                                        className={classNames(
                                            projectPlatePageStyle.rightSide,
                                            projectPlatePageStyle.prostoFont
                                        )}
                                    >
                                        <div className={projectPlatePageStyle.fontLowerCase}>
                                            A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            {/*asdasd*/}

                            <div className={projectPlatePageStyle.projectDevider} />

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/pixli/pixli-proj.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    2020*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Pixli
                                    </div>
                                    <Link href="/projects/7">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                            <div className={projectPlatePageStyle.mouseHolder}>
                                <MouseIcon />
                            </div>

                        </div>

                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts