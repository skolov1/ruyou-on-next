import projectPlatePageStyle from '../ProjectPlate.module.scss'
import someBigamStyles from './someBigamStyles.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { Modal } from '../../../components/complexes/Modal'
import { FC, useCallback, useState, useContext } from 'react'
import Bigam from '../../../public/projects/logo/bigam-logo-icon.svg'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { StylesContext } from '../../_app'
// import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch'


type Props = {}

const slides: string[] = [
    '/projects/bigam/bigam-pic-2.png',
]

const Contacts: FC<Props> = () => {

    const [contentForModal, setContentForModal] = useState<any>()
    const { projectPlatePageStyle } = useContext(StylesContext)
    const onHandleCatchImage = useCallback((e) => {
        setContentForModal(e.target.currentSrc)
    }, [setContentForModal])

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект Бигам-бонусная карта | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/bigam/bigam-proj.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2020</div>*/}
                            <Bigam className={projectPlatePageStyle.projectLogo} />
                            <p className={projectPlatePageStyle.projectDescription}></p>
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>
                        {/* <div className={projectPlatePageStyle.mainWrapCentered}> */}

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>О проекте</div>
                            <p className={projectPlatePageStyle.paragraph}>
                            Приложение «Бигам-бонусная карта» - электронная бонусная карта сети центров инструмента и техники «Бигам».
                            Единое мобильное приложение, объединяющее в себе несколько параллельных бонусных программ
                            </p>
                            <div className={projectPlatePageStyle.pointTitle}>Технологический стек</div>
                            <p className={projectPlatePageStyle.paragraph}>
                            Кроссплатформенное мобильное приложение 
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                <div className={projectPlatePageStyle.developParams}>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>1140</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                    </DevelopCounter>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>2 разработча</div>
                                        <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 Аккаунт-Менеджер</div>
                                    </DevelopCounter>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                <div className={projectPlatePageStyle.colorsWrap}>

                                    <div className={projectPlatePageStyle.sideWrap}>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#191919' }} />
                                            Primary Text & Icons
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#5A5A5A' }} />
                                            Secondary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#8D8C8C' }} />
                                            Tertiary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#B3B3B3' }} />
                                            Disabled Text & Placeholder
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#E0E0E0' }} />
                                            Border
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#F5F5F5' }} />
                                            Gray Background
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#FFFFFF' }} />
                                            White Background
                                        </div>
                                    </div>

                                    <div className={projectPlatePageStyle.mainWrap}>
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#004584' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#1C609F' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#4B7EAE' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#819FBB' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#BBC7D2' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#4B75BA' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#5B82C1' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#809CC9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#A7B8D4' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#CFD7E5' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F97C00' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F59D45' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F7B068' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F9C692' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FCDEBF' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F9B34E' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FAC171' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FBCE8E' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FCDBAE' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FDEBD2' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#45A163' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#EB5D47' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FADF7E' }} />
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                <div className={projectPlatePageStyle.leftSide}>
                                    <div className={someBigamStyles.fontHeader}>Helvetica</div>
                                    <div className={someBigamStyles.fontUpperCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={someBigamStyles.fontLowerCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={someBigamStyles.fontNumber}>1  2  3  4  5  6  7  8  9  0</div>
                                </div>

                                <div className={projectPlatePageStyle.rightSide}>
                                    <div className={someBigamStyles.fontHdr}>Header</div>
                                    <div className={someBigamStyles.fontTitle}>Title</div>
                                    <div className={someBigamStyles.fontSubtitle}>Subtitle</div>

                                    <div className={someBigamStyles.bodyOne}>
                                        <span style={{paddingRight: '45px'}}>Body 1</span>
                                        <span style={{fontWeight: 600}}>Body 1 - Medium</span>
                                    </div>
                                    <div className={someBigamStyles.bodyTwo}>
                                        <span style={{paddingRight: '47px'}}>Body 2</span>
                                        <span style={{fontWeight: 600}}>Body 2 - Medium</span>
                                    </div>
                                    <div className={someBigamStyles.caption}>
                                        <span>Caption</span>
                                    </div>
                                    <div className={someBigamStyles.button}>
                                        <span>Button</span>
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.itemHolderFullScreen)}>
                                <div className={projectPlatePageStyle.pointTitle}>Прототипы приложения</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Wireframes</p>
                            </div>
                            <img className={projectPlatePageStyle.imageForHuge} src="/projects/bigam/bigam-prototypes.svg" />

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>
                                    Дизайн
                                </div>
                                <p className={projectPlatePageStyle.addPointTitle}>Design</p>
                                <div className={projectPlatePageStyle.mobileDesign}>
                                    <img src='/projects/bigam/bigam-mobile-design-1.png' />
                                    <img src='/projects/bigam/bigam-mobile-design-2.png' />
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointSecondTitle}>Задача по дизайну</div>
                                <div className={projectPlatePageStyle.paragraphInColumn}> 
                                    <p className={someBigamStyles.paragraphLeft}>
                                        Быть максимально простым и близким к народу, так как сеть магазинов Бигам - это именно народные магазины
                                    </p>
                                    <p className={someBigamStyles.paragraphRight}>
                                    В приложении соблюдена стилистика уже знакомая клиентам сети по банерам, рекламным материалам, оформлению магазинов.
                                     Мы старались обеспечить 100% приемственность
                                    </p>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <ProjectImageSlider slides={slides} />
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.displaced)}>
                                <div className={projectPlatePageStyle.pointTitle}>Компоненты</div>
                                <img
                                    className={projectPlatePageStyle.imageForHuge}
                                    src="/projects/bigam/bigam-libs-image.svg"
                                />
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.itemHolderFullScreen)}>
                                <div className={projectPlatePageStyle.pointSecondTitle}>Картографирование процессов</div>
                                <p className={projectPlatePageStyle.pointSecondTitleAdd}>Картографирование процессов позволяет учесть 
                                все пожелания клиента и избежать ошибок при проектировании.</p>
                            </div>
                            <img className={projectPlatePageStyle.imageScheme} src="/projects/bigam/bigam-logic-scheme.svg"/>
                            <div className={projectPlatePageStyle.itemHolder}>
                                <p className={projectPlatePageStyle.paragraphAbout}>
                                Приложение «Бигам-бонусная карта» - электронная бонусная карта сети центров инструмента и техники «Бигам» в твоем смартфоне.
                                </p>
                                <p className={projectPlatePageStyle.paragraphAbout}>
                                Регистрируйте индивидуальную электронную бонусную карту в приложении "Бигам-бонусная карта" и пользуйтесь всеми преимуществами
                                 программы лояльности без необходимости хранить пластиковую карту.
                                </p>
                                <p className={projectPlatePageStyle.paragraphAbout}>
                                После регистрации в приложении отображается:
                                </p>
                                <ul className={classNames(projectPlatePageStyle.listProject,projectPlatePageStyle.dashedList)}>
                                    <li className={projectPlatePageStyle.listProjectItem}>электронная бонусная/дисконтная карта для предъявления на кассе;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>текущее количество накопленных бонусов;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>история операций по начислению/списанию бонусов.</li>
                                </ul>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.projectDevider} />
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/g-group/g-group-background.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    2020*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        G-group
                                    </div>
                                    <Link href="/projects/1">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>


                        <div className={projectPlatePageStyle.mouseHolder}>
                            <MouseIcon />
                        </div>

                        {/* </div> */}

                    </div>


                    <Footer minimal />

                </section>

            </main>

            {contentForModal && false &&
                <Modal>
                    <img
                        className={projectPlatePageStyle.imageInModal}
                        src={contentForModal}
                    />
                </Modal>
            }

        </motion.div>
    )
}
export default Contacts

/*
<TransformWrapper velocityAnimation={{ disabled: false }}>
    <TransformComponent>
    </TransformComponent>
</TransformWrapper>

*/