// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Pixli from '../../../public/projects/logo/pixli-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/pixli/pixli-pic-3.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект Pixli | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/pixli/pixli-proj.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2020</div>*/}
                            <Pixli className={projectPlatePageStyle.projectLogo} />
                            <p className={projectPlatePageStyle.projectDescription}>
                                Профессиональная платформа для визуального
                                digital-дизайна и frontend-разработки
                            </p>
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>
                        <div className={projectPlatePageStyle.mainWrapCentered}>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Создать конструктор для опытных профессиональных дизайнеров,
                                    который по своим возможностям максимально приближен к ручной верстке.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                PIXLI - это не просто конструктор. Это мощный инструмент, позволяющий быстро создавать красивые
                                , анимированные сайты, которые не под силу создать даже JS-программистам с уровнем middle+
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>12506</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>6 разработчиков</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>2 дизайнера</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>2 тестировщика</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>2 Менеджера проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>


                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Современная анимация реализована на платформе без необходимости работы с JavaScript.
                                    Быстрая загрузка у пользователя благодаря оптимизации чистого кода.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                    В Pixli можно создать множество разных эффектов, настроить сложное
                                    взаимодействие между элементами и изменение их стилей и свойств,
                                    управлять этими функциями на временной шкале.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                    <b>Триггеры</b>: с помощью триггеров элементы сайта
                                    могут взаимодействовать между собой, например,
                                    можно создать опрос на сайте и при нажатии на
                                    кнопку запустится таймер, а когда время выйдет,
                                    пользователь это увидит, а данные формы будут
                                    отправлены администратору сайта.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                    <b>Максимальная свобода с CMS</b>: на платформе уже встроена
                                    визуальная CMS с динамическим управлением контентом,
                                    но пользователям также доступны безлимитный экспорт
                                    созданных сайтов на внешние хостинги, интеграция с
                                    любыми CMS и Framework платформами, включение любых
                                    скриптов в код на PIXLI. Предусмотрено скачивание
                                    архива сайта.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                    <b>Можно подключить собственное доменное имя</b> и
                                    SSL-сертификат, и сайт будет идеально оптимизирован
                                    под все виды поисковых систем.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.imageProjectHolder}>
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-1.png" alt="" />
                                        <img className={projectPlatePageStyle.imageProject} src="/projects/pixli/pixli-pic-2.png" alt="" />
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointSecondTitle}>
                                    Pixli успешно экономит драгоценное время разработчиков:
                                </div>
                                <ul className={projectPlatePageStyle.projectList}>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Многопользовательский доступ - изменения в проект могут вносить одновременно несколько разработчиков
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Копирование между проектами - для однотипных проектов повторяющиеся элементы можно просто скопировать
                                    </li>
                                    <li className={projectPlatePageStyle.projectListItem}>
                                        Функция White Label - передача сайта заказчику через отдельный личный кабинет
                                    </li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                    <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                    <div className={projectPlatePageStyle.colorsWrap}>

                                        <div className={projectPlatePageStyle.sideWrap}>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#333333' }} />
                                                Primary Text
                                            </div>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#292F3B' }} />
                                                Secondary Text
                                            </div>
                                            <div className={projectPlatePageStyle.sideWrapItem}>
                                                <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#C9C9C9' }} />
                                                Tertiary Text
                                            </div>
                                        </div>

                                        <div className={projectPlatePageStyle.mainWrap}>
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#4F42E9' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#7166F9' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#8F86F9' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#B7B1FE' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D5D1FF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#850BFF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#AB56FF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C184FF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D6ACFF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E2C6FF' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F1AF51' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FBC170' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F2C27E' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F9D39C' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFE8C8' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FF50B5' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FF73C4' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FF80C9' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFA5D9' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFCAE9' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E04F5F' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#EC6E7C' }} />
                                            <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FD9EA9' }} />
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                    <div className={projectPlatePageStyle.leftSide}>
                                        <div className={projectPlatePageStyle.fontHeader}>Montserrat</div>
                                        <div className={projectPlatePageStyle.fontUpperCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                        <div className={projectPlatePageStyle.fontLowerCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                        <div className={projectPlatePageStyle.fontNumber}>1  2  3  4  5  6  7  8  9  0</div>
                                    </div>

                                    <div className={projectPlatePageStyle.rightSide}>
                                        <div className={projectPlatePageStyle.fontH1}>H1</div>
                                        <div className={projectPlatePageStyle.fontH2}>H2</div>
                                        <div className={projectPlatePageStyle.fontH3}>H3</div>

                                        <div className={projectPlatePageStyle.fontGroup}>
                                            <span>Body 1</span>
                                            <span>Body 1 - Medium</span>
                                        </div>
                                        <div className={projectPlatePageStyle.fontGroup}>
                                            <span>Body 2</span>
                                            <span>Body 2 - Medium</span>
                                        </div>
                                        <div className={projectPlatePageStyle.fontGroup}>
                                            <span>Button</span>
                                        </div>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slides} />
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.projectDevider} />

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/metro/metro-proj.png)' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    2020*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Metro
                                    </div>
                                    <Link href="/projects/8">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                            <div className={projectPlatePageStyle.mouseHolder}>
                                <MouseIcon />
                            </div>

                        </div>

                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts