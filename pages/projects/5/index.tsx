// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import someOnRelaxStyles from './someOnRelaxStyles.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import OnRelax from '../../../public/projects/logo/onrelax-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/on-relax/on-relax-pic-1.png',
    '/projects/on-relax/on-relax-pic-2.png',
    '/projects/on-relax/on-relax-pic-3.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект Янаотдых | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/on-relax/on-relax-background.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2021</div>*/}
                            <OnRelax className={projectPlatePageStyle.projectLogo} />
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder} style={{marginBottom:'40px'}}>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                            <p className={projectPlatePageStyle.paragraph} style={{maxWidth:'890px'}}>
                            Разработать портал для бронирования путевок в детские оздоровительные лагеря с возможностью авоматической проверки 
                            прав пользователей на получение государственных субсидий и льгот.
                            Проработать Визульную составляющую проекта: нейминг, лого, цвета, брендбук
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder} style={{marginBottom:'40px',marginTop:'0px'}}>
                                <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                <div className={projectPlatePageStyle.developParams}>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>790</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                    </DevelopCounter>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                        <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                    </DevelopCounter>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder} style={{marginBottom:'40px'}}>
                            <div className={projectPlatePageStyle.pointTitle}>Функционал</div>
                            <p className={projectPlatePageStyle.paragraph} style={{maxWidth:'890px'}}>
                            Возможность подать заявку в лагерь, оплатить через эквайринг сбербанка, связаться с модераторами через чат, 
                            купить билеты на поезд/самолёт до лагеря, совершить возврат путевки. 
                            Настроена фильтрация по лагерям, интеграция с 1С
                            </p>
                        </div>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>О проекте</div>
                            <p className={projectPlatePageStyle.paragraph} style={{maxWidth:'890px',margin:0}}>
                            Локальный интернет портал по организации субсидируемого детского и молодёжного отдыха – информационная система, 
                            которая позволяет  автоматизировать процесс  подачи заявок на мероприятия, в лагеря и смены, позволяя реализовать 
                            право на получение льгот и субсидий в «один клик» и по принципу «одного окна».
                            </p>
                            <p className={projectPlatePageStyle.paragraph} style={{maxWidth:'890px',margin:0}}>
                            Процедура подачи заявок происходит в считанные минуты в зависимости от скорости набора символов заявителей. 
                            Модераторы муниципальных образований Ямало-Ненецкого автономного округа получают целостную картину статистических данных и 
                            принимают участие в информационном обмене, а именно в обработке заявок, создания лагерей, смен, мероприятий.
                            </p>
                            <p className={projectPlatePageStyle.paragraph} style={{maxWidth:'890px',margin:'0 0 20px 0'}}>
                            Кроме того, система создана для повышения качества информирования граждан о действующих событиях в сфере отдыха, 
                            оздоровления и досуга в Ямало-Ненецком автономном округе.
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <ProjectImageSlider slides={slides} />
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 6px 0',fontFamily:'Gotham Pro',fontWeight:500,color:"white"}}>
                            Технологический стек</p>
                            <p className={projectPlatePageStyle.paragraph} style={{margin:0,fontFamily:'Lato',fontWeight:'normal'}}>
                            Frontend: React&Redux;
                            </p>
                            <p className={projectPlatePageStyle.paragraph} style={{margin:0,fontFamily:'Lato',fontWeight:'normal'}}>
                            Backend: Golang
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder} style={{marginBottom:'175px'}}>
                                <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                <div className={projectPlatePageStyle.colorsWrap}>

                                    <div className={projectPlatePageStyle.sideWrap}>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#3B3B3B' }} />
                                            Primary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#5A5A5A' }} />
                                            Secondary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#D7D7D7' }} />
                                            Tertiary Text
                                        </div>
                                    </div>

                                    <div className={projectPlatePageStyle.mainWrap}>
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F25632' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FE7353' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FC9179' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFB8A8' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFD0C6' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F8881A' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FC9632' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFB66F' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFD3A8' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFE6CD' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#6D67DF' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#8782EE' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#9792F5' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#BAB6F9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D1CEFE' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#3E86E8' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#62A1F5' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#7CACED' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#A0C1EE' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C3D7F1' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#1B9DEF' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#4BB3F5' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#77C4F4' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#A6D4F1' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#C5E7FC' }} />
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                <div className={projectPlatePageStyle.leftSide}>
                                    <div className={projectPlatePageStyle.fontHeader}>Inter</div>
                                    <div className={projectPlatePageStyle.fontUpperCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={projectPlatePageStyle.fontLowerCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={projectPlatePageStyle.fontNumber}>1  2  3  4  5  6  7  8  9  0</div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.projectDevider} />
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/suenco/suenco-proj.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    2021*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Суэнко
                                    </div>
                                    <Link href="/projects/6">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.mouseHolder}>
                            <MouseIcon />
                        </div>


                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts