// import projectPlatePageStyle from './Projects.module.scss'
import { Head } from '../../components/complexes/Head'
import { Header } from '../../components/complexes/Header'
import { Footer } from '../../components/complexes/Footer'
import { SideSocial } from '../../components/complexes/SideSocial'
import SliderItem from '../../components/complexes/SliderProjectsItem/SliderItem'
import { FC, useCallback, useEffect, useState, useContext, useRef } from 'react'
import MouseIcon from '../../public/mouse-icon.svg'
import MoveIcon from '../../public/move-slides-icon.svg'
import Link from 'next/link'
import { motion } from 'framer-motion'
import { StylesContext } from './../_app'

type Props = {}

const Contacts: FC<Props> = ({

}) => {
    const refSlide = useRef<any>()

    const { projectsPageStyle } = useContext(StylesContext)

    const [sliderData, setSliderData] = useState({ countSlides: 0, currentSlide: 1 })

    const onHandleChangeSlide = useCallback((
        current: number, // don't use
        next: number
    ) => {
        setSliderData({
            ...sliderData,
            currentSlide: next,
        })
    }, [setSliderData, sliderData])

    const onHandleInitSlide = useCallback((countSlides: number) => {
        setSliderData({
            ...sliderData,
            countSlides: countSlides
        })
    }, [setSliderData, sliderData])

    const handleChangeSlide = useCallback((toSlide: number) => {
        if (refSlide && refSlide.current && refSlide.current.slickGoTo) {
            refSlide.current.slickGoTo(toSlide - 1)
        }
    }, [setSliderData, sliderData])

    const lastWheelAt = useRef(new Date())
    const onHandleMouseWheelMove = useCallback((e) => {
        const now = new Date()
        if (now.getTime() - lastWheelAt.current.getTime() < 30) {
            lastWheelAt.current = now
            return
        }
        lastWheelAt.current = now

        const delta = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? e.deltaX * -1 : e.deltaY

        if (refSlide && refSlide.current && refSlide.current.slickGoTo) {
            if (delta > 0) {
                if (sliderData.currentSlide <= 1)
                    return
                refSlide.current.slickPrev()
            } else {
                if (sliderData.currentSlide >= sliderData.countSlides)
                    return
                refSlide.current.slickNext()
            }
        }
    }, [sliderData, setSliderData])

    return (
        <motion.div
            className={projectsPageStyle.container}
            animate={{ opacity: 1 }}
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
            transition={{ duration: .3 }}
        >

            <Head
                title={"Более 80 проектов за 9 лет | RuYou"}
                description={"Мы разработали множество проектов в сфере ритейла, e-commerce, nocode решений и других сферах"}
            />

            <main className={projectsPageStyle.main}>
                <Header />

                <section
                    onWheel={onHandleMouseWheelMove}
                    className={projectsPageStyle.wrapper}
                >
                    {/*<Link href={`/projects/${sliderData.currentSlide}`}>*/}
                    {/*    <div className={projectsPageStyle.watchProjects}>*/}
                    {/*        Смотреть проект*/}
                    {/*    </div>*/}
                    {/*</Link>*/}

                    <div className={projectsPageStyle.sliderHolder}>
                        <SliderItem
                            refSlide={refSlide}
                            onInitSlide={onHandleInitSlide}
                            onChangeSlide={onHandleChangeSlide}
                            setCurrentNumberSlide={sliderData.currentSlide}
                        />
                    </div>

                    <div className={projectsPageStyle.buttonPanel}>
                        {/*<Link href="/projects">*/}
                        {/*    <span className={projectsPageStyle.backButton}>*/}
                        {/*        Назад*/}
                        {/*    </span>*/}
                        {/*</Link>*/}
                        <MouseIcon className={projectsPageStyle.mouseIcon} />

                        <div className={projectsPageStyle.mobileMoveIcon}>
                            <MoveIcon />
                        </div>

                    </div>

                    <SideSocial />

                </section>

                <Footer
                    slidesAmount={sliderData.countSlides}
                    currentSlide={sliderData.currentSlide}
                    onChange={handleChangeSlide}
                />

            </main>

        </motion.div>
    )
}
export default Contacts
