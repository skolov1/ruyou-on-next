// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import someGgroupStyles from './someGgroupStyles.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Ggroup from '../../../public/projects/logo/agroup-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import MouseIcon from '../../../public/mouse-icon.svg'
import classNames from 'classnames'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'


type Props = {}

const slides: string[] = [
    '/projects/g-group/g-group-pic-1.png',
]

const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проект G-Group АГЗС | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/g-group/g-group-background.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2021</div>*/}
                            <Ggroup className={projectPlatePageStyle.projectLogo} />
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                            <p className={projectPlatePageStyle.paragraph}>
                            Разработать приложение с бонусной системой для заправок АГЗС. Обеспечить возможность регулировать начисление бонусов через админ панель 
                            (время суток, объем заказа, по конкретной станции, по статусу пользователя и др.
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                <div className={projectPlatePageStyle.developParams}>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>790</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                    </DevelopCounter>
                                    <DevelopCounter>
                                        <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                        <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                        <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                    </DevelopCounter>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                                <p className={projectPlatePageStyle.paragraph} style={{ marginTop: 20 }}>
                                G-Group АГЗС - Широкая сеть АГЗС в городе Екатеринбурге и городах-спутниках, расположенная
                                в Екатеринбурге, Первоуральске, Сысерти, Арамиле, Малых Брусянах, Каменск-Уральске.
                                </p>

                                <p className={projectPlatePageStyle.paragraph} style={{ margin:0}}>
                                    Приложение "G-Group АГЗС" позволяет:
                                </p>

                                <ul className={classNames(projectPlatePageStyle.listProject,projectPlatePageStyle.dashedList)}>
                                    <li className={projectPlatePageStyle.listProjectItem}>контролировать бонусные баллы на своем счете;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>отслеживать баланс;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>вести историю начислений бонусов и их списание;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>первыми узнавать об акциях и новостях на наших АГЗС;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>видеть всю карту заправочных станций сети "G-Group";</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>построить маршрут до выбранной станции;</li>
                                    <li className={projectPlatePageStyle.listProjectItem}>оставлять отзывы</li>
                                </ul>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>
                                    Дизайн
                                </div>
                                <p className={projectPlatePageStyle.addPointTitle}>Design</p>
                                <div className={projectPlatePageStyle.mobileDesign}>
                                    <img src='/projects/g-group/g-group-design-1.png' />
                                    <img src='/projects/g-group/g-group-design-2.png' />
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.pointTitle}>
                                Результат
                            </div>
                            <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 11px 0',fontFamily:'Gotham Pro',fontWeight:'normal'}}>Функционал</p>
                            <ul className={projectPlatePageStyle.listProject} style={{paddingLeft: '13px',marginBottom: '19px',fontFamily:'Lato',fontWeight:'normal'}}>
                                <li className={projectPlatePageStyle.listProjectItem}>Баланс бонусов</li>
                                <li className={projectPlatePageStyle.listProjectItem}>QR код для начисления бонусов</li>
                                <li className={projectPlatePageStyle.listProjectItem}>История начислений/списаний бонусов</li>
                                <li className={projectPlatePageStyle.listProjectItem}>Чат</li>
                                <li className={projectPlatePageStyle.listProjectItem}>Гео карта Google</li>
                                <li className={projectPlatePageStyle.listProjectItem}>Построение маршрута на карте до ближайшей АГЗС</li>
                                <li className={projectPlatePageStyle.listProjectItem}>Интеграция сервиса Яндекс.Заправки</li>
                                <li className={projectPlatePageStyle.listProjectItem}>Новостная лента</li>
                            </ul>
                            <p className={projectPlatePageStyle.addPointTitle} style={{margin:'0 0 6px 0',fontFamily:'Gotham Pro',fontWeight:'normal'}}>Технологический стек</p>
                            <p className={projectPlatePageStyle.paragraph} style={{margin:0,fontFamily:'Lato',fontWeight:'normal'}}>
                                <span style={{fontFamily:'Lato',fontWeight:500}}>Frontend:</span> JavaScript, React Native
                            </p>
                            <p className={projectPlatePageStyle.paragraph} style={{margin:0,fontFamily:'Lato',fontWeight:'normal'}}>
                                <span style={{fontFamily:'Lato',fontWeight:500}}>Backend:</span> PHP, Yii2
                            </p>
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <ProjectImageSlider slides={slides} />
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder} style={{marginBottom:'175px'}}>
                                <div className={projectPlatePageStyle.pointTitle}>Цвета и шрифты</div>
                                <p className={projectPlatePageStyle.addPointTitle}>Colors & Typography</p>

                                <div className={projectPlatePageStyle.colorsWrap}>

                                    <div className={projectPlatePageStyle.sideWrap}>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#3B3B3B' }} />
                                            Primary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#5A5A5A' }} />
                                            Secondary Text
                                        </div>
                                        <div className={projectPlatePageStyle.sideWrapItem}>
                                            <span className={projectPlatePageStyle.colorSideHolder} style={{ backgroundColor: '#D7D7D7' }} />
                                            Tertiary Text
                                        </div>
                                    </div>

                                    <div className={projectPlatePageStyle.mainWrap}>
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#76227F' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#9740A0' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#B977C0' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D799DE' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#E5C1E9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#009241' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#259D5A' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#4ACE85' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#79D9A4' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#AEEBC9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#A6A6A6' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#BFBFBF' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#D9D9D9' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#F5F5F5' }} />
                                        <span className={projectPlatePageStyle.colorMainWrapItem} style={{ backgroundColor: '#FFFFFF' }} />
                                    </div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={classNames(projectPlatePageStyle.itemHolder, projectPlatePageStyle.fontHolder)}>
                                <div className={projectPlatePageStyle.leftSide} style={{maxWidth:'580px'}}>
                                    <div className={someGgroupStyles.fontHeader}>SF UI Display</div>
                                    <div className={someGgroupStyles.fontUpperCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={someGgroupStyles.fontLowerCase}>A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  B  W  X  Y  Z</div>
                                    <div className={someGgroupStyles.fontNumber}>1  2  3  4  5  6  7  8  9  0</div>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.itemHolder}>
                            <div className={projectPlatePageStyle.projectDevider} />
                        </div>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={projectPlatePageStyle.itemHolder}>
                                <div
                                    className={projectPlatePageStyle.nextProject}
                                    style={{ backgroundImage: 'url(/projects/on-relax/on-relax-background.png' }}
                                >
                                    {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                    {/*    Готовится к релизу 2021*/}
                                    {/*</div>*/}
                                    <div className={projectPlatePageStyle.logoProject}>
                                        Я на отдых
                                    </div>
                                    <Link href="/projects/5">
                                        <div className={projectPlatePageStyle.watchProject}>
                                            Смотреть проект
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </ScrollAnimation>

                        <div className={projectPlatePageStyle.mouseHolder}>
                            <MouseIcon />
                        </div>


                    </div>


                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts