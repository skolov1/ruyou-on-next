// import projectPlatePageStyle from '../ProjectPlate.module.scss'
import { Head } from '../../../components/complexes/Head'
import { Header } from '../../../components/complexes/Header'
import { Footer } from '../../../components/complexes/Footer'
import Novokur from '../../../public/projects/logo/novokur-logo-icon.svg'
import { DevelopCounter } from '../../../components/complexes/DevelopCounter'
import { ProjectImageSlider } from '../../../components/complexes/ProjectImageSlider'
import { FC, useContext } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll';
import { ReviewClient } from '../../../components/complexes/ReviewClient'
import { StylesContext } from '../../_app'
import MouseIcon from '../../../public/mouse-icon.svg'

// Logos
import BadenLogo from '../logos/baden-logo-tape.svg'
import EyelashLogo from '../logos/eyelash-logo-tape.svg'
import NovokurLogo from '../logos/novokur-logo-tape.svg'
import PivkoLogo from '../logos/pivko-logo-tape.svg'
import ScripturesLogo from '../logos/scriptures-logo-tape.svg'
import TeleplusLogo from '../logos/teleplus-logo-tape.svg'
import ZartexLogo from '../logos/zartex-logo-tape.svg'



type Props = {}

const slidesNovokur: string[] = [
    '/projects/common/novokur-slider-1.png',
]

const slidesBaden: string[] = [
    '/projects/common/baden-slider-1.png',
]

const slidesEyelash: string[] = [
    '/projects/common/eyelash-slider-1.png',
]

const slidesPivko: string[] = [
    '/projects/common/pivko-slider-1.png',
]

const slidesScriptures: string[] = [
    '/projects/common/scriptures-slider-1.png',
]

const slidesTeleplus: string[] = [
    '/projects/common/teleplus-slider-1.png',

]

const slidesZartex: string[] = [
    '/projects/common/zartex-slider-1.png',
]
const Contacts: FC<Props> = () => {

    const { projectPlatePageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={projectPlatePageStyle.container}
            animate={{ opacity: 1 }}
        >

            <Head
                title={"Проекты Ново-Курьинская, Баден-Баден, Теле плюс, Испытай писания, ПивКо | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <main className={projectPlatePageStyle.main}>
                <Header />

                <section className={projectPlatePageStyle.projectSection} >

                    <div className={projectPlatePageStyle.projectPreview}>
                        <img className={projectPlatePageStyle.projImage} src='/projects/ural2019-2021proj.png' alt="" />
                        <div className={projectPlatePageStyle.infoHolder}>
                            {/*<div className={projectPlatePageStyle.projectYear}>2017-2020</div>*/}
                            <Novokur className={projectPlatePageStyle.projectLogo} />
                        </div>
                        <ul className={projectPlatePageStyle.stackList}>
                            <li className={projectPlatePageStyle.stackListItem}>Mobile app</li>
                            <li className={projectPlatePageStyle.stackListItem}>UI/UX design</li>
                            <li className={projectPlatePageStyle.stackListItem}>Front-end</li>
                            <li className={projectPlatePageStyle.stackListItem}>Back-end</li>
                        </ul>
                    </div>


                    <div className={projectPlatePageStyle.descriptionHolder}>
                        <div className={projectPlatePageStyle.mainWrapCentered}>

                            <div className={projectPlatePageStyle.itemHolder}>

                                <NovokurLogo className={projectPlatePageStyle.logoProjectForTape} />

                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Создать удобное, быстрое, интуитивно понятное приложение для заказа воды,
                                    возможность быстро выбрать из удобного каталога свою любимую воду и другие
                                    товары, оформить заказ с возможностью выбрать дату и время доставки,
                                    выбрать вариант оплаты (карта или наличные), иметь возможность оставить
                                    обратную связь (оценить качество, оставить отзыв)
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>2100</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>3 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>


                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Красивое, удобное (наши дизайнеры и менеджер проекта лично прошли тест
                                    на терпение, проверив более 100 экранов и сделав около 2000 правок), в котором есть
                                </p>

                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Личный кабинет </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Каталог продукции и корзина покупателя </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Подборка актуальных акций </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Возможность выбрать способ оплаты и интеграция с платежной системой </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Интеграция с CRM </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Возможность делать оповещения для пользователя </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Возможность собирать обратную связь </li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesNovokur} />
                                </div>
                            </ScrollAnimation>

                            
                            <div className={projectPlatePageStyle.itemHolder}>
                                <BadenLogo className={projectPlatePageStyle.logoProjectForTape} />
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Презентовать комплекс как лучшее место для семейного
                                    отдыха в Екатеринбурге, создать удобный для клиентов
                                    сервис с широким функционалом, вывести комплекс в топ по продажам
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>550</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Сайт с высокой конверсией, которую мы
                                    обеспечили за счет удобного функционала:
                                </p>

                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Калькулятор с расчетом стоимости туров </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Интегрированная CRM с отображением текущей загрузки и наличия свободныx мест в реальном времени </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Интерактивные и анимационные блоки </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Каталог услуг, акций и информационный раздел </li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesBaden} />
                                </div>
                            </ScrollAnimation>

                           

                            <div className={projectPlatePageStyle.itemHolder}>
                                <TeleplusLogo className={projectPlatePageStyle.logoProjectForTape} />
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Создать интерфейс для абонентов, который позволит управлять услугами прямо с экрана смартфона.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>1230</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>3 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    С помощью приложения легко получить доступ к камерам видеонаблюдения,
                                    любимым телеканалам, принять участие в программе лояльности и получить
                                    онлайн консультацию от техподдержки, то есть воспользоваться всем
                                    функционалом, который мы в него включили:
                                </p>

                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Доступ к 350 камерам услуги «Безопасный двор» </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Подключение услуг Интернет и IP-TV  </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Личный кабинет для управления услугами   </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Магазин   </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Мое ТВ. Online-управление ТВ-каналами   </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Обратная связь. Чат   </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Программа лояльности   </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Мой домофон. Возможность видеть на экране смартфона звонящих в домофон с любой точки планеты   </li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesTeleplus} />
                                </div>
                            </ScrollAnimation>

                            

                            {/*<div className={projectPlatePageStyle.itemHolder}>
                                <ZartexLogo className={projectPlatePageStyle.logoProjectForTape} />
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Охватить новую аудиторию, привлечь ее интересным и удобным,
                                    понятным приложением, побудить к покупке, повысив тем самым
                                    продажи. Получить конкурентное преимущество, адаптировав
                                    свой бизнес для продаж в интернете (привлечение трафика с
                                    мобильных устройств обходится дешевле и покупатели, приходящие
                                    таким образом чаще совершают покупки и лояльнее настроены к
                                    бренду - тут отлично сработал отдел маркетинга заказчика)
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>1830</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>6 разработчиков</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>2 дизайнера</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Проект-Менеджер</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Так как ранее мы работали над созданием нескольких ресурсов
                                    для Zartex, сфера деятельности была нам понятна и знакома.
                                    Мы очень поддерживаем клиентов, которые решают выходить в
                                    онлайн поэтапно, это упрощает задачу им и дает возможность
                                    наблюдать за процессом нам. Мы создали приложение, в котором
                                    есть личный кабинет с историей покупок, формой для обратной
                                    связи, каталог с товарами, возможность детально рассмотреть
                                    каждое ковровое покрытие, выбрать цвет и другие параметры.
                                    Шаг по внедрению мобильного приложения был полностью оправдан,
                                    судя по обратной связи от нашего клиента, трафик, ведущий
                                    за собой продажи, существенно увеличился и мы достигли своей цели.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesZartex} />
                                </div>
                            </ScrollAnimation>

                            

                            <div className={projectPlatePageStyle.itemHolder}>
                                <EyelashLogo className={projectPlatePageStyle.logoProjectForTape} />
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Удобное и понятное приложение для обмена опытом, общения,
                                    с возможностью завести личный кабинет, общаться на форуме
                                    и задавать интересующие вопросы.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>210</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Проект-Менеджер</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Приложение для iOS и Android, в котором есть справочник,
                                    орум, возможность настроить свой профиль. Push-уведомления,
                                    авторизация через социальные сети. Приложение работает
                                    быстро и имеет постоянную связь с сервером.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesEyelash} />
                                </div>
                                    </ScrollAnimation>*/}

                            

                            <div className={projectPlatePageStyle.itemHolder}>
                                <ScripturesLogo className={projectPlatePageStyle.logoProjectForTape} />
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Создать удобное приложение с возможностью прослушивания аудиокниг
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>210</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>2 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 Менеджер проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Приложение посвящено трудам Е.А. Авдеенко, объединяет
                                    его единомышленников и позволяет читателям общаться
                                    с автором. Книги и лекции можно скачать на телефон
                                    или читать онлайн. Что можно делать в приложении:
                                </p>

                                <ul className={projectPlatePageStyle.listProject}>
                                    <li className={projectPlatePageStyle.listProjectItem}>Слушать аудиокниги через встроенный аудиоплеер (он был написал с нуля именно под этот проект) </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Покупать книги </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Напрямую связываться с автором </li>
                                    <li className={projectPlatePageStyle.listProjectItem}>Искать книги и делать «закладки» </li>
                                </ul>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesScriptures} />
                                </div>
                            </ScrollAnimation>

                            
                            <div className={projectPlatePageStyle.itemHolder}>
                                <PivkoLogo className={projectPlatePageStyle.logoProjectForTape} />
                                <div className={projectPlatePageStyle.pointTitle}>Задача</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Сделать максимально доступным большой объем обучающей информации,
                                    создать единую удобную площадку для общения франчайзи и их сотрудников.
                                    Организовать гибкую систему обучения франчайзи и их сотрудников;
                                    обеспечить возможность ведения обучающих курсов с онлайн-тестированием
                                    по каждому уроку; собирать статистические данные о прохождении
                                    обучающих программ (контроль качества); система рейтингов магазинов и франчайзи
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div className={projectPlatePageStyle.pointTitle}>Объем работ</div>
                                    <div className={projectPlatePageStyle.developParams}>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>510</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>Часов работы</div>
                                        </DevelopCounter>
                                        <DevelopCounter>
                                            <div className={projectPlatePageStyle.developParamsHead}>3 разработчика</div>
                                            <div className={projectPlatePageStyle.developParamsPlus}>+</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 дизайнер</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>1 тестировщик</div>
                                            <div className={projectPlatePageStyle.developParamsBody}>2 Менеджера проектов</div>
                                        </DevelopCounter>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.itemHolder}>
                                <div className={projectPlatePageStyle.pointTitle}>Результат</div>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Поставленные задачи были решены успешно, по всем пунктам
                                    они были выполнены и заказчик получил необходимый продукт.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Сервисом удобно пользоваться, он существенно упростил коммуникацию
                                    между пользователями,  система тестирования и сбора информации
                                    показала себя максимально эффективной и комфортной, она также
                                    предоставляет отчеты и статистику о результатах обучения.
                                </p>
                                <p className={projectPlatePageStyle.paragraph}>
                                    Портал адресно оповещает пользователей и новостях компании, новости
                                    разделены на категории по географическому признаку - в зависимости
                                    от города, где находится пользователь. Каждый обучающий материал
                                    имеет персональное назначение в зависимости от типа пользователя,
                                    его рейтинга в компании и геолокации.
                                </p>
                            </div>

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <ProjectImageSlider slides={slidesPivko} />
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.projectDevider} />

                            <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                                <div className={projectPlatePageStyle.itemHolder}>
                                    <div
                                        className={projectPlatePageStyle.nextProject}
                                        style={{ backgroundImage: 'url(/projects/small/small-pic-1.png' }}
                                    >
                                        {/*<div className={projectPlatePageStyle.yearProject}>*/}
                                        {/*    2021*/}
                                        {/*</div>*/}
                                        <div className={projectPlatePageStyle.logoProject}>
                                            Small
                                        </div>
                                        <Link href="/projects/4">
                                            <div className={projectPlatePageStyle.watchProject}>
                                                Смотреть проект
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </ScrollAnimation>

                            <div className={projectPlatePageStyle.mouseHolder}>
                                <MouseIcon />
                            </div>
                        </div>

                    </div>

                    <Footer minimal />

                </section>

            </main>

        </motion.div>
    )
}
export default Contacts