import { ServicesHeader } from '../../../components/complexes/ServicesHeader'
import { QuoteBlock } from '../../../components/complexes/QuoteBlock'
import { FC } from 'react'
import { FormOrder } from '../../../components/complexes/FormOrder'
import Link from 'next/link'
import ScrollAnimation from 'react-animate-on-scroll';
import { StylesContext } from '../../_app'
import { useContext } from 'react'
import { Footer } from '../../../components/complexes/Footer'
import Head from "../../../components/complexes/Head/Head";


type Props = {}

const Services: FC<Props> = () => {

    const { servicesPageStyle } = useContext(StylesContext)

    return (
        <div
            className={servicesPageStyle.wrapper}
        >
            <Head
                title={"Разработка интернет магазинов и маркетплейсов | RuYou"}
                description={"Разработка интернет магазинов и маркетплейсов и комплексных IT решений под ключ"}
            />

            <div style={{position:'relative',zIndex:1}}>
                <ServicesHeader
                    actionWordsCount={1}
                    hugeTitle={'eCommerce \n все для электронной торговли'}
                    currentServices={3}
                />
            </div>

            <div className={servicesPageStyle.bodyWrapper} style={{position:'relative',zIndex:0}}>
                <div className={servicesPageStyle.bodyCentered} style={{paddingBottom:'0px'}}>

                    <h1 className={servicesPageStyle.servicesTitle} style={{marginBottom:'58px',maxWidth:'950px'}}>
                        eCommerce интернет-магазины, маркетплейсы и агрегаторы.
                    </h1>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'29px',maxWidth:'850px'}}>
                        Компания RuYou обладает немалым опытом по созданию сложных, высоконагруженных сервисов, способных справиться с любыми задачами.
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'29px'}}>
                        Мы поможем вам создать и / или проинтегрировать с существующей ИТ-инфраструктурой компании все составляющие части вашего бизнеса:
                    </p>

                    <ul className={servicesPageStyle.servicesList} style={{marginBottom:'53px'}}>
                        <li className={servicesPageStyle.servicesListItem}>Фронт-часть: сайт интернет-магазина, мобильное приложение</li>
                        <li className={servicesPageStyle.servicesListItem}>Бэк-офис: биллинг-системы, системы учета товаров (услуг), ERP-системы</li>
                        <li className={servicesPageStyle.servicesListItem}>Систему управления взаимоотношениями с клиентами (CRM)</li>
                        <li className={servicesPageStyle.servicesListItem}>Службы доставки</li>
                        <li className={servicesPageStyle.servicesListItem}>Платежные системы</li>
                        <li className={servicesPageStyle.servicesListItem}>Любые сторонние сервисы</li>
                    </ul>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{marginBottom:'26px'}}>
                        Преимущества индивидуальных решений от компании RuYou
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Полная свобода в выборе архитектурного решения
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Полная свобода интеграций
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Возможность создавать рекомендательные системы на основе коллаборативной фильтрации или подключая нейросети
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Подключение любых сторонних сервисов
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Разработка бонусных программ
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Подключение кэшбек-сервисов
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'20px'}}>
                        Глубокая интеграция с существующей ИТ-инфраструктурой компании
                    </p>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{marginBottom:'20px'}}>
                        Развивается все, что может быть дистанционным
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Развитие интернет торговли итак было достаточно бурным, а 2020 год добавил темпов роста, увеличив их на порядок. 
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'31px'}}>
                        Современный бизнес стоит перед выбором - отдать свою клиентскую базу пионерам ИТ-бизнеса ЯндексЛавке, 
                        Самокату и другим сервисам или развивать свои решения. Выбор очевиден.
                    </p>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{marginBottom:'31px'}}>
                        Процент проникновения электронной коммерции
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <div className={servicesPageStyle.servicesImageWrapper}>
                            <img className={servicesPageStyle.servicesImage} src="/images/ecommerce-pic.png" alt="example" style={{maxWidth:'710px',maxHeight:'643px',marginBottom:'0px'}}/>
                        </div>
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Наш опыт в области создания сложных высоконагруженных проектов и в области автоматизации 
                        бизнеса позволит вам создать уникальный для рынка продукт, действуя на опережение с конкурентами.
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'40px'}}>
                        Мы готовы к полному погружению в ваш проект еще на этапе presale. 
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <QuoteBlock mw="800px" colorTheme="yellow" text="
                        Вы можете подключиться к ЯндексЛавке и Самокату, они с удовольствием добавят ваших клиентов к своим, изучат пользовательские предпочтения, 
                        сформируют ассортиментную матрицу, построят свои логистические центры и после выкинут вас и ваш бизнес на свалку истории.
                        " />
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginTop:'49px',marginBottom:'0px'}}>
                        Есть проект? - звоните, наши менеджеры подробно проконсультируют и ответят на все интересующие вопросы. 
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'80px'}}>
                        Есть ТЗ - присылайте. Мы проводим предварительную оценку и подбираем архитектурное решение в течении дня. 
                    </p>

                </div>

                <div className={servicesPageStyle.mobileOtherServices}>
                    <div className={servicesPageStyle.mobileOtherServicesTitle}>Другие услуги</div>
                    <Link href="/services/1">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Мобильные приложения
                        </div>
                    </Link>
                    <Link href="/services/2">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Web-сервисы
                        </div>
                    </Link>
                    <Link href="/services/3">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Дизайн
                        </div>
                    </Link>
                    <Link href="/projects">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Смотреть портфолио
                        </div>
                    </Link>
                </div>

                <div className={servicesPageStyle.formHolder}>
                    <FormOrder />
                </div>

            </div>

            <Footer minimal />

        </div>
    )
}
export default Services