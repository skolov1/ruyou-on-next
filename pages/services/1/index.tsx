
import { ServicesHeader } from '../../../components/complexes/ServicesHeader'
import { QuoteBlock } from '../../../components/complexes/QuoteBlock'
import { FC, useContext } from 'react'
import { FormOrder } from '../../../components/complexes/FormOrder'
import classNames from 'classnames'
import Link from 'next/link'
import ScrollAnimation from 'react-animate-on-scroll';
import { StylesContext } from './../../_app'
import { Footer } from '../../../components/complexes/Footer'
import Head from "../../../components/complexes/Head/Head";

type Props = {}

const Services: FC<Props> = (props) => {

    const { servicesPageStyle } = useContext(StylesContext)

    return (
        <div
            className={servicesPageStyle.wrapper}
        >
            <Head
                title={"Разработка мобильных приложений | RuYou"}
                description={"Разработка мобильных приложений и комплексных IT решений под ключ"}
            />

            <div className={servicesPageStyle.headerStyle}>
                <ServicesHeader
                    actionWordsCount={1}
                    hugeTitle={"Разработка \n мобильных  приложений"}
                    currentServices={0}
                />
            </div>

            <div className={classNames(servicesPageStyle.bodyWrapper,servicesPageStyle.bodyStyle)}>
                <div className={servicesPageStyle.bodyCentered}>

                    <h1 className={servicesPageStyle.servicesTitle}>
                        Разработка мобильных приложений iOS и Android
                    </h1>

                    <h2 className={servicesPageStyle.servicesSmallTitle}>
                        Полный цикл разработки от идеи до публикации в магазинах приложений
                    </h2>

                    <p className={servicesPageStyle.servicesHeadHeading}>
                        Создайте мобильное приложение для клиентов / сотрудников / любых пользователей
                    </p>
                    <ul className={servicesPageStyle.servicesList}>
                        <li className={servicesPageStyle.servicesListItem}>Торговля и производство (ритейл, производство, оказание услуг и многие другие сферы)</li>
                        <li className={servicesPageStyle.servicesListItem}>Управление персоналом (автоматизация бизнес-процессов внутри компании)</li>
                        <li className={servicesPageStyle.servicesListItem}>Клиентские сервисы (медиа-сервисы)</li>
                    </ul>

                    <p className={servicesPageStyle.servicesHeadHeading}>
                        Кроссплатформенные решения на React Native
                    </p>
                    <p className={classNames(
                        servicesPageStyle.servicesListItem,
                        servicesPageStyle.servicesListItemMobile
                    )}>Преимущества для вас:</p>
                    <ul className={servicesPageStyle.servicesList}>
                        <li className={servicesPageStyle.servicesListItem}>Скорость разработки</li>
                        <li className={servicesPageStyle.servicesListItem}>Экономия бюджета разработки по сравнению с традиционной "нативной" разработкой до 30%</li>
                        <li className={servicesPageStyle.servicesListItem}>Популярный, легкоподдерживаемый стек</li>
                    </ul>

                    <p className={servicesPageStyle.servicesHeadHeading}>
                        Нативные мобильные приложения
                    </p>
                    <p className={servicesPageStyle.servicesListItem}>Золотой стандарт мобильной разработки, принятый во всем мире:</p>
                    <ul className={servicesPageStyle.servicesList}>
                        <li className={servicesPageStyle.servicesListItem}>Java или Kotlin для платформы Android</li>
                        <li className={servicesPageStyle.servicesListItem}>Swift для платформы iOS.</li>
                    </ul>
                    <p className={servicesPageStyle.servicesListItem}>
                        Преимущества нативной разработки - возможность максимально корректного
                        использования аппаратных функций устройства, от камеры до пульсометра
                    </p>


                    <h1 className={servicesPageStyle.servicesTitle}>Почему с нами просто?</h1>
                    <p className={servicesPageStyle.servicesListItem}>
                        Мы поддерживаем широкий стек технологий и предложим
                        вам несколько альтернатив, чтобы вы смогли выбрать
                        оптимальный подход к решению любой задачи.
                    </p>

                    <img className={servicesPageStyle.servicesImage} src="/images/service-mobile-1.png" alt=""/>

                    <h1 className={servicesPageStyle.servicesHeadHeading}>
                        Гибкость в управлении процессом разработки.
                    </h1>
                    <p className={servicesPageStyle.servicesListItem} style={{ maxWidth: 'unset' }}>
                        -    Agile (SCRUM) - максимально гибкий процесс разработки, для проектов, которые живут и развиваются прямо сейчас, и новые идеи необходимо претворять в жизнь не дожидаясь окончания разработки.<br />
                        -    Работа по ТЗ с фиксированным бюджетом - для проектов, где заказчик кристально четко понимает, что именно он хочет видеть результатом<br /> разработки, и все, даже мелкие детали, способен описать на старте.
                    </p>

                    <h1 className={servicesPageStyle.servicesHeadHeading}>Открытость и профессионализм.</h1>
                    <p className={servicesPageStyle.servicesListItem} style={{ maxWidth: 'unset' }}>
                        Вам достаточно поделиться идеей, и мы предоставим вам все
                        возможные альтернативы по разработке. Проконсультируем, если
                        необходимо, вместе с вами решим сложные вопросы по концепции продукта.
                        Вы всегда можете рассчитывать на нашу помощь и поддержку.
                    </p>

                    <h1 className={servicesPageStyle.servicesHeadHeading}>Командный подход к реализации любого проекта</h1>
                    <p className={servicesPageStyle.servicesListItem} style={{marginBottom:'15px'}}>
                        Разделение труда позволяет получить исключительное
                        качество результата работы на каждом этапе реализации проекта
                    </p>
                    <img className={servicesPageStyle.servicesImage} src="/images/service-mobile-2.png" alt="" />
                    <h1 className={servicesPageStyle.servicesHeadHeading}>В команде проекта участвуют</h1>
                    <p className={servicesPageStyle.servicesListItem} style={{ margin: '20px 0', maxWidth: 'unset' }}>
                        <strong>Бизнес-аналитик</strong> <br />
                        - полное погружение в ваш бизнес и подробный разбор существующих бизнес-процессов.
                        Мы НЕ автоматизируем хаос, мы совместно с вами оптимизируем текущие бизнес процессы,
                        и только после этого проектируем ИТ-решение
                    </p>
                    <p className={servicesPageStyle.servicesListItem} style={{ margin: '20px 0', maxWidth: 'unset' }}>
                        <strong>Менеджер проекта</strong> <br />
                        Собираем подробные пользовательские истории (user story),
                        проектируем и гибко управляем проектом по SCRUM,  Agile,
                        что позволяет гибко подходить к процессу разработки,
                        добиваясь максимальной эффективности
                    </p>
                    <p className={servicesPageStyle.servicesListItem} style={{ margin: '20px 0', maxWidth: 'unset' }}>
                        <strong>UX / UI проектирование</strong><br />
                        Детальная проработка интерфейсов, позволяет сделать приложение
                        интуитивно понятным  и простым в управлении. Только позитивный
                        пользовательский опыт.
                    </p>

                    <p className={servicesPageStyle.servicesListItem} style={{ margin: '20px 0', maxWidth: 'unset' }}>
                        <strong>Программисты</strong> <br />
                        Программисты - эта наша гордость. В нашей команде работают только настоящие профи.
                        Вы убедитесь в этом, получив их резюме перед стартом работ.
                    </p>

                    <p className={servicesPageStyle.servicesListItem} style={{ margin: '20px 0', maxWidth: 'unset' }}>
                        <strong>Тестировщик</strong><br />
                        Автотесты и ручное тестирование после каждого этапа разработки - залог устойчивой и качественной работы в будущем
                    </p>

                    <p className={servicesPageStyle.servicesListItem} style={{ margin: '20px 0', maxWidth: 'unset' }}>
                        <strong>Аккаунт менеджер</strong><br />
                        Это ваш личный помощник, который помнит все, что вы говорили,
                        не упустит ни малейшей детали. Его задача быть в курсе и держать
                        руку на пульсе<br /> всего проекта. Это тот самый человек,
                        который готов поддержать и развивать ваши идеи, даже если вам
                        непременно нужно обсудить их в 2 часа ночи.
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <QuoteBlock mw="800px" colorTheme="black" text="
                            Качественный продукт начинается
                            с качественной организации
                            процесса разработки."
                        />
                    </ScrollAnimation>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <QuoteBlock mw="830px" colorTheme="yellow" text="
                            Аккаунт-менеджер, это ваш друг,
                            с которым можно обсужать идеи и
                            делиться сомнениями даже в 2 часа ночи"
                        />
                    </ScrollAnimation>

                    <p
                        className={servicesPageStyle.servicesSimpleText}
                        style={{ marginTop: '20px', maxWidth: 'unset' }}
                    >
                        Разработка мобильного приложения ответственный и ресурсоемкий
                        шаг, требующий серьезной экспертизы. Звоните или пишите, мы с
                        удовольствием проконсультируем вас, или даже проведем скоринговое
                        проектирование еще на этапе pre-sale
                    </p>
                </div>

                <div className={servicesPageStyle.mobileOtherServices}>
                    <div className={servicesPageStyle.mobileOtherServicesTitle}>Другие услуги</div>
                    <Link href="/services/2">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Web-сервисы
                        </div>
                    </Link>
                    <Link href="/services/3">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Дизайн
                        </div>
                    </Link>
                    <Link href="/services/4">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            eCommerce
                        </div>
                    </Link>
                    <Link href="/projects">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Смотреть портфолио
                        </div>
                    </Link>
                </div>

                <div className={servicesPageStyle.formHolder}>
                    <FormOrder />
                </div>

            </div>

            <Footer minimal />

        </div>
    )
}
export default Services
