import { ServicesHeader } from '../../../components/complexes/ServicesHeader'
import { QuoteBlock } from '../../../components/complexes/QuoteBlock'
import { FC } from 'react'
import { FormOrder } from '../../../components/complexes/FormOrder'
import Link from 'next/link'
import ScrollAnimation from 'react-animate-on-scroll';
import { StylesContext } from '../../_app'
import { useContext } from 'react'
import { Footer } from '../../../components/complexes/Footer'
import Head from "../../../components/complexes/Head/Head";


type Props = {}

const Services: FC<Props> = () => {

    const { servicesPageStyle } = useContext(StylesContext)

    return (
        <div
            className={servicesPageStyle.wrapper}
        >
            <Head
                title={"Разработка продуктового дизайна | RuYou"}
                description={"Разработка продуктового дизайна. UX/UI дизайн, Motion дизайн"}
            />

            <div style={{position:'relative',zIndex:1}}>
                <ServicesHeader
                    actionWordsCount={1}
                    hugeTitle={'Создаём\n продуктовый дизайн'}
                    currentServices={2}
                />
            </div>


            <div className={servicesPageStyle.bodyWrapper} style={{position:'relative',zIndex:0}}>
                <div className={servicesPageStyle.bodyCentered} style={{paddingBottom:'0px'}}>

                    <h1 className={servicesPageStyle.servicesTitle} style={{marginBottom:'53px'}}>
                        Дружелюбный дизайн
                    </h1>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{marginBottom:'26px'}}>
                        UI вовсе не против UX
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'15px'}}>
                        Для нас дизайн начинается с продуманного интерфейса, а не с красивой картинки. Удобство использования ставится во главу угла. Всегда.
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'50px'}}>
                        Для Вас, как для нашего Клиента работа с нами начнется с написания пользовательских сценариев и только потом мы совместно приступим к
                        разработке мокапов сервиса/приложения. Основная задача мокапов, уже
                        на этапе проектирования предусмотреть и заранее продумать  и протестировать расположение всех элементов управления.
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <div className={servicesPageStyle.servicesImageWrapper} style={{marginBottom:'51px'}}>
                            <p className={servicesPageStyle.servicesImageTitle}>
                                Полноценные мокапы вместо карандашных набросков
                            </p>
                            <img className={servicesPageStyle.servicesImage} src="/images/design-pic.png" alt="example" style={{maxWidth:'1280px',maxHeight:'728px'}}/>
                            <p className={servicesPageStyle.servicesImageDescription}>Пример мокапов</p>
                        </div>
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        При проектировании мы учитываем не только собственный опыт, но и рисуем портреты конечных пользователей,
                        чтобы знать наверняка их привычки и пользовательское поведение.
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'0px'}}>
                        Если у вас нет видения, каким должен быть ваш сервис или мобильное приложение мы поможем вам его сформировать,
                        так как готовы погружаться в проект и не только для расчета стоимости, но и для того, чтобы ваше видение проекта становилось более детальным.
                    </p>
                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'32px'}}>
                        Мы готовы создавать мокапы еще на этапе pre-sale для более четкого понимания задачи и нами и вами.
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <QuoteBlock mw="835px" colorTheme="black"
                            text="
                                Продуктовый дизайнер - это творец только во вторую очередь, в первую очередь - он исследователь,
                                скрупулезно изучающий поведение ваших пользователей
                            "
                        />
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{margin:'51px 0 26px 0'}}>
                        А теперь UI
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginBottom:'50px'}}>
                        Время для отрисовки “красивостей” наступает на самом последнем этапе дизайна, а иногда - не наступает никогда)).
                        Несколько раз клиенты еще на этапе прорисовки мокапов говорили: “Этого более чем достаточно. Как раз то, что нам нужно”
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <QuoteBlock mw="800px" colorTheme="yellow"
                            text="
                            Создавать дизайн, который вдохновляет - основная задача наших дизайнеров
                            "
                        />
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesSimpleText} style={{margin:'51px 0 36px 0'}}>
                        Поделитесь своими мыслями, чувствами, образами - и уже через пару дней вы увидите созданный нами концепт, который вам точно понравится.
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <div className={servicesPageStyle.servicesImageWrapper} style={{marginBottom:'2px'}}>
                            <img className={servicesPageStyle.servicesImage} src="/images/services-example.png" alt="example" style={{maxWidth:'1280px',maxHeight:'728px'}}/>
                        </div>
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{marginBottom:'51px'}}>
                        Дизайнер RuYou в поисках вдохновения
                    </p>
                </div>

                <div className={servicesPageStyle.mobileOtherServices}>
                    <div className={servicesPageStyle.mobileOtherServicesTitle}>Другие услуги</div>
                    <Link href="/services/1">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Мобильные приложения
                        </div>
                    </Link>
                    <Link href="/services/2">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Web-сервисы
                        </div>
                    </Link>
                    <Link href="/services/4">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            eCommerce
                        </div>
                    </Link>
                    <Link href="/projects">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Смотреть портфолио
                        </div>
                    </Link>
                </div>

                <div className={servicesPageStyle.formHolder}>
                    <FormOrder />
                </div>

            </div>

            <Footer minimal />

        </div>
    )
}
export default Services
