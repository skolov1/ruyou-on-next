import { ServicesHeader } from '../../../components/complexes/ServicesHeader'
import { QuoteBlock } from '../../../components/complexes/QuoteBlock'
import { FC } from 'react'
import { FormOrder } from '../../../components/complexes/FormOrder'
import Link from 'next/link'
import ScrollAnimation from 'react-animate-on-scroll';
import { StylesContext } from './../../_app'
import { useContext } from 'react'
import { Footer } from '../../../components/complexes/Footer'
import Head from "../../../components/complexes/Head/Head";


type Props = {}

const Services: FC<Props> = () => {

    const { servicesPageStyle } = useContext(StylesContext)

    return (
        <div
            className={servicesPageStyle.wrapper}
        >
            <Head
                title={"Разработка web сервисов и приложений под ключ | RuYou"}
                description={"Разработка web сервисов под ключ и комплексных IT решений"}
            />

            <div style={{position:'relative',zIndex:1}}>
            <ServicesHeader
                actionWordsCount={1}
                hugeTitle={"Разработка\n web-приложений"}
                currentServices={1}
            />
            </div>

            <div className={servicesPageStyle.bodyWrapper} style={{position:'relative',zIndex:0}}>
                <div className={servicesPageStyle.bodyCentered} style={{paddingBottom:'0px'}}>

                    <h1 className={servicesPageStyle.servicesTitle} style={{maxWidth:'2000px'}}>
                        Разработка онлайн-сервисов, SaaS-решения и интеграции любой сложности.
                    </h1>

                    <p className={servicesPageStyle.servicesHeadHeading}>
                        Полный спектр ИТ решений для вашего бизнеса
                    </p>
                    <ul className={servicesPageStyle.servicesList} style={{marginBottom:'87px'}}>
                        <li className={servicesPageStyle.servicesListItem}>Личный кабинет сотрудника/клиента</li>
                        <li className={servicesPageStyle.servicesListItem}>SaaS решения</li>
                        <li className={servicesPageStyle.servicesListItem}>Аггрегаторы</li>
                        <li className={servicesPageStyle.servicesListItem}>Маркетплейсы</li>
                        <li className={servicesPageStyle.servicesListItem}>Электронная торговля</li>
                        <li className={servicesPageStyle.servicesListItem}>Биржи</li>
                        <li className={servicesPageStyle.servicesListItem}>Туристические сервисы</li>
                        <li className={servicesPageStyle.servicesListItem}>Банковские приложения</li>
                        <li className={servicesPageStyle.servicesListItem}>Логистические сервисы</li>
                        <li className={servicesPageStyle.servicesListItem}>Корпоративные порталы и интранет сервисы</li>
                    </ul>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <div className={servicesPageStyle.servicesImageWrapper}>
                            <img className={servicesPageStyle.servicesImage} src="/images/web-pic.png" alt="example" style={{maxWidth:'1280px',maxHeight:'690px',marginBottom:'17px'}}/>
                            <p className={servicesPageStyle.servicesImageDescription} style={{marginBottom:'66px'}}>Сервис по продаже субсидируемых путевок  для департамента молодежной политики ЯНАО</p>
                        </div>
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesHeadHeading} style={{marginBottom:'27px'}}>
                        Последовательность работы
                    </p>

                    <ul className={servicesPageStyle.servicesList} style={{listStyleType:'decimal',marginBottom:'55px'}}>
                        <li className={servicesPageStyle.servicesListItem}>Предпроектное исследование  с полным погружением на этапе pre-sale</li>
                        <li className={servicesPageStyle.servicesListItem}>Максимально детализированная оценка проекта перед стартом (подробная смета)</li>
                        <li className={servicesPageStyle.servicesListItem}>Нулевой спринт, по окончании которого вы получите
                            <p>
                                Инфарх проекта (информационная архитектура)
                            </p>
                            <p>
                                Детализированные мокапы экранов/страниц сервиса
                            </p>
                            <p>
                                Подробные UserStory к каждому экрану (детально описанные сценарии взаимодействия пользователя с каждым экраном)
                            </p>
                            <p>
                                Спринт-план реализации с четкими сроками
                            </p>
                            <p>
                                Дизайн экранов на следующий спринт
                            </p>
                            <p>
                                Логин к YouTrack - мы подключим вас к нашему таск-менеджеру, чтобы вы могли наблюдать и контролировать весь процесс разработки.
                            </p>
                        </li>
                        <li className={servicesPageStyle.servicesListItem}>Поэтапная разработка</li>
                        <li className={servicesPageStyle.servicesListItem}>Релиз проекта</li>
                    </ul>

                    <p className={servicesPageStyle.servicesHeadHeading}>
                        Широкий технологический стек
                    </p>

                    <p className={servicesPageStyle.servicesSimpleText} style={{margin: '8px 0 40px 0'}}>
                        Нативные (Kotlin и Swift)  и кроссплатформенные (React Native) решения для мобильных устройств, ReactJS для веб-приложений, 
                        PHP или GO для высоконагруженных сервисов на бэкенде, от PostgreSQL до MariaDB - 
                        мы обладаем всем спектром технологий для реализации вашего проекта, каким бы сложным он не был
                    </p>

                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <QuoteBlock colorTheme="black" mw="800px" text="
                        Мы обладаем полным спектром технологий для разработки самых сложных IT-систем" />
                    </ScrollAnimation>

                    <p className={servicesPageStyle.servicesSimpleText} style={{marginTop:'40px',marginBottom:'96px'}}>
                        Разработка сложного сервиса это  ответственный и ресурсоемкий шаг, требующий серьезной экспертизы. 
                        Звоните или пишите, мы с удовольствием проконсультируем вас, или даже проведем скоринговое проектирование еще на этапе pre-sale. 
                        Это бесплатно и ни к чему вас не обязывает. 
                    </p>

                </div>


                <div className={servicesPageStyle.mobileOtherServices}>
                    <div className={servicesPageStyle.mobileOtherServicesTitle}>Другие услуги</div>
                    <Link href="/services/1">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Мобильные приложения
                        </div>
                    </Link>
                    <Link href="/services/3">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Дизайн
                        </div>
                    </Link>
                    <Link href="/services/4">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            eCommerce
                        </div>
                    </Link>
                    <Link href="/projects">
                        <div className={servicesPageStyle.mobileOtherServicesItem}>
                            Смотреть портфолио
                        </div>
                    </Link>
                </div>

                <div className={servicesPageStyle.formHolder}>
                    <FormOrder />
                </div>

            </div>

            <Footer minimal />

        </div>
    )
}
export default Services