import styles from '../styles/Home.module.scss'
import { Head } from '../components/complexes/Head'
import { Footer } from '../components/complexes/Footer'
import { Header } from '../components/complexes/Header'
import { HomeSlider } from '../components/complexes/HomeSlider'
import {FC, useContext, useState} from 'react'
import { Slides } from '../types'
import { Try3D } from '../components/complexes/Try3D'
import { motion } from 'framer-motion'
import {StylesContext} from "./_app";


const slides: Slides[] = [
  { title: 'Разработка \n мобильных  приложений', link: '/services/1' },
  { title: 'Разработка \n web-приложений', link: '/services/2' },
  { title: 'Создаём \n продуктовый дизайн', link: '/services/3' },
  { title: 'eCommerce \n все для электронной торговли', link: '/services/4' },
  { title: 'Смотреть \nпортфолио', link: '/projects' },
]

type Props = {}

const Home: FC<Props> = () => {

  const [currentSlide, setCurrentSlide] = useState(1)

  const { homePageStyle } = useContext(StylesContext)

  return (
    <motion.div
        className={homePageStyle.container}
        exit={{ opacity: 0 }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: .3 }}
    >
      <Head
          title={"Разработка мобильных и web приложений | RuYou"}
          description={"Разработка мобильных приложений и комплексных IT решений под ключ"}
      />

      <main className={homePageStyle.main}>
        <Header />

        <Try3D slide={currentSlide} />

        <HomeSlider
          slidePosition={currentSlide}
          slides={slides}
          onChange={setCurrentSlide}
        />

        <div className={styles.footerWrap}>
          <Footer
              slidesAmount={slides.length}
              currentSlide={currentSlide}
              onChange={setCurrentSlide}
          />
        </div>

      </main>
    </motion.div>
  )
}
export default Home