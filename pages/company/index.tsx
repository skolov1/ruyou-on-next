// import companyPageStyle from './Company.module.scss'
import { Head } from '../../components/complexes/Head'
import { Header } from '../../components/complexes/Header'
import { FactsCounter } from '../../components/complexes/FactsCounter'
import { TapeLine } from '../../components/complexes/TapeLine'
import { TeamItem } from '../../components/complexes/TeamItem'
import { Footer } from '../../components/complexes/Footer'
import MouseIcon from '../../public/mouse-icon.svg'
import { SideSocial } from '../../components/complexes/SideSocial'
import { FC } from 'react'
import { motion } from 'framer-motion'
import ScrollAnimation from 'react-animate-on-scroll'
import { useContext } from 'react'
import { StylesContext } from './../_app'
import TeamSlider from '../../components/complexes/TeamSlider/TeamSlider'

type Props = {}

const Company: FC<Props> = () => {

    let { companyPageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={companyPageStyle.container}
            animate={{ opacity: 1 }}
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
        >

            <Head
                title={"Мы профессиональная и дружная команда, любящая свою работу | RuYou"}
                description={"Мы ценим наших заказчиков, сотрудников и партнеров, строим всю работу на создании эффективных и доступных IT решений"}
            />

            <main className={companyPageStyle.main}>
                <Header />

                <section className={companyPageStyle.viewportDescription}>
                    <div className={companyPageStyle.centerAlignment}>
                        <p className={companyPageStyle.goldText}>Кто мы?</p>
                        <h1 className={companyPageStyle.titleMain}>RuYou - команда<br />увлеченных людей</h1>
                    </div>
                    <SideSocial />
                </section>

                <section className={companyPageStyle.viewportInfo}>
                    <div className={companyPageStyle.centerAlignment}>
                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <h1 className={companyPageStyle.header}>Ruyou</h1>
                            <p className={companyPageStyle.paragraph}>
                                RUYOU - это команда. Это люди, которые
                                действительно получают удовольствие от того,
                                что делают. Это профессионалы, которые постоянно
                                учатся новому, ловят тренды, ищут новые решения
                                и не боятся сложностей. Это программисты,
                                маркетологи, аналитики, менеджеры. Мы -
                                IT-команда, с которой вам легко идти к успеху.
                            </p>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <div className={companyPageStyle.frameMission}>
                                <p className={companyPageStyle.companyMissionHeader}>миссия компании</p>
                                <h1 className={companyPageStyle.companyMissionText}>Сделать качественный IT-продукт доступным</h1>
                            </div>

                            <p className={companyPageStyle.paragraph}>
                                Мы делаем качественный, современный IT-продукт
                                доступным по стоимости для широкого круга клиентов.
                                Каждый бизнес должен иметь возможность развиваться
                                за счет внедрения IT-решений. Ваши инвестиции должны
                                окупаться многократно, в этом мы видим основной
                                смысл работы с нами.
                            </p>
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                            <p className={companyPageStyle.paragraphFacts}>
                                Немного фактов о нас
                            </p>
                            <div className={companyPageStyle.factHolder}>
                                <FactsCounter topValue={7} caption={'Лет работы'} />
                                <FactsCounter topValue={86} caption={'Проектов'} />
                                <FactsCounter topValue={25} caption={'Сотрудников'} />
                            </div>
                        </ScrollAnimation>
                    </div>
                </section>

                <section className={companyPageStyle.viewportTeam}>
                    <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
                        <div className={companyPageStyle.centerAlignment}>
                            <h1 className={companyPageStyle.header}>Наша команда</h1>
                            <div className={companyPageStyle.teamHolder}>
                                <TeamItem name="Вячеслав Семенец" profession="Директор" photo="/company/photo-company-1.jpg" />
                                <TeamItem name="Константин Мисюра" profession="Менеджер проектов" photo="/company/photo-company-2.jpg" />
                                <TeamItem name="Максим Синицин" profession="iOS|Android разработчик" photo="/company/photo-company-3.jpg" />
                                <TeamItem name="Константин Гетманский" profession="Коммерческий директор" photo="/company/photo-company-4.jpg" />
                                <TeamItem name="Ирина Каширова" profession="Операционный директор" photo="/company/photo-company-5.jpg" />
                                <TeamItem name="Николай Дебелов" profession="Руководитель отдела back-end разработки" photo="/company/photo-company-6.jpg" />
                                <TeamItem name="Евгений Забегаев" profession="Менеджер проектов" photo="/company/photo-company-7.jpg" />
                                <TeamItem name="Владислав Родионов" profession="iOS разработчик" photo="/company/photo-company-8.jpg" />
                                <TeamItem name="Александра Кондратьева" profession="Тестировщик" photo="/company/photo-company-9.jpg" />
                                <TeamItem name="Эрнест Шагибалов" profession="Android разработчик" photo="/company/photo-company-10.jpg" />
                                <TeamItem name="Наталия Иовлева" profession="Frontend разработчик" photo="/company/photo-company-11.jpg" />
                                <TeamItem name="Юрий Селиверстов" profession="PHP/Python разработчик" photo="/company/photo-company-12.jpg" />
                                <TeamItem name="Андрей Балашов" profession="Frontend разработчик" photo="/company/photo-company-13.jpg" />
                                <TeamItem name="Сергей Михряков" profession="Руководитель отдела front-end разработки" photo="/company/photo-company-14.jpg" />
                                <TeamItem name="Екатерина Сидорова" profession="UI/UX Дизайнер" photo="/company/photo-company-15.jpg" />
                                <TeamItem name="Юля Волкова" profession="HR" photo="/company/photo-company-16.jpg" />
                                <TeamItem name="Павел Кулаков" profession="Аккаунт-менеджер" photo="/company/photo-company-17.jpg" />
                            </div>
                        </div>
                    </ScrollAnimation>
                </section>

                {/* <div className={companyPageStyle.mouseHolder}>
                    <MouseIcon />
                </div> */}

                <Footer minimal />
            </main>

        </motion.div>
    )
}
export default Company
