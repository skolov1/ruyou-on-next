import { Provider } from 'react-redux'
import {Head } from "next/document";
import { store } from '../store/index'
import "animate.css/animate.compat.css";
import '../styles/globals.scss'
import { AnimatePresence, motion } from 'framer-motion'
import { createContext } from 'react';

// Import styles from other component for prevent removing (bug framer motion).
// Or delirium that knows no boundaries......
import homePageStyle from '../styles/Home.module.scss'
import companyPageStyle from './company/Company.module.scss'
import contactsPageStyle from './contacts/Contacts.module.scss'
import orderPageStyle from './order/Order.module.scss'
import projectPlatePageStyle from './projects/ProjectPlate.module.scss'
import projectsPageStyle from './projects/Projects.module.scss'
import servicesPageStyle from './services/Services.module.scss'
import exampleStyle from './example/Example.module.scss'

import lightDropFilesStyle from './../components/complexes/OrderStepsContainer/LightDropFiles/LightDropFiles.module.scss'
import lightMaskTextFieldStyle from './../components/complexes/OrderStepsContainer/LightMaskTextField/LightMaskTextField.module.scss'
import LightSelectStyle from './../components/complexes/OrderStepsContainer/LightSelect/LightSelect.module.scss'
import lightTextFieldStyle from './../components/complexes/OrderStepsContainer/LightTextField/LightTextField.module.scss'
import orderStepsContainerStyle from './../components/complexes/OrderStepsContainer/OrderStepsContainer.module.scss'
import developerCounterStyle from './../components/complexes/DevelopCounter/DevelopCounter.module.scss'
import dropFilesStyle from './../components/complexes/DropFiles/DropFiles.module.scss'
import factsCounterStyle from './../components/complexes/FactsCounter/FactsCounter.module.scss'
import footerStyle from './../components/complexes/Footer/Footer.module.scss'
import formOrderStyle from './../components/complexes/FormOrder/FormOrder.module.scss'
import fullScreenMenuStyle from './../components/complexes/FullScreenMenu/FullScreenMenu.module.scss'
import headerStyle from './../components/complexes/Header/Header.module.scss'
import homeSliderStyle from './../components/complexes/HomeSlider/HomeSlider.module.scss'
import mainSlideStyle from './../components/complexes/MainSlide/MainSlide.module.scss'
import modalStyle from './../components/complexes/Modal/Modal.module.scss'
import openedCityStyle from './../components/complexes/OpenedCity/OpenedCity.module.scss'
import projectImageSliderStyle from './../components/complexes/ProjectImageSlider/ProjectImageSlider.module.scss'
import quoteBlockStyle from './../components/complexes/QuoteBlock/QuoteBlock.module.scss'
import reviewClientStyle from './../components/complexes/ReviewClient/ReviewClient.module.scss'
import servicesHeaderStyle from './../components/complexes/ServicesHeader/ServicesHeader.module.scss'
import sideSocialStyle from './../components/complexes/SideSocial/SideSocial.module.scss'
import stepClickerStyle from './../components/complexes/StepClicker/StepClicker.module.scss'
import tapeLineStyle from './../components/complexes/TapeLine/TapeLine.module.scss'
import teamItemStyle from './../components/complexes/TeamItem/TeamItem.module.scss'
import teamSliderStyle from './../components/complexes/TeamSlider/TeamSlider.module.scss'
import viewportStyle from './../components/complexes/Viewport/Viewport.module.scss'
import burgerButtonStyle from './../components/simples/BurgerButton/BurgerButton.module.scss'
import checkBoxStyle from './../components/simples/CheckBox/CheckBox.module.scss'
import maskTextFieldStyle from './../components/simples/MaskTextField/MaskTextField.module.scss'
import selectStyle from './../components/simples/Select/Select.module.scss'
import textFieldStyle from './../components/simples/TextField/TextField.module.scss'


import try3DModules from './../components/complexes/Try3D/Try3D.module.scss'


const criticalStyles = {
  homePageStyle,
  companyPageStyle,
  contactsPageStyle,
  orderPageStyle,
  projectPlatePageStyle,
  projectsPageStyle,
  servicesPageStyle,
  exampleStyle,

  lightDropFilesStyle,
  lightMaskTextFieldStyle,
  LightSelectStyle,
  lightTextFieldStyle,
  orderStepsContainerStyle,
  developerCounterStyle,
  dropFilesStyle,
  factsCounterStyle,
  footerStyle,
  formOrderStyle,
  fullScreenMenuStyle,
  headerStyle,
  homeSliderStyle,
  mainSlideStyle,
  modalStyle,
  openedCityStyle,
  projectImageSliderStyle,
  quoteBlockStyle,
  reviewClientStyle,
  servicesHeaderStyle,
  sideSocialStyle,
  stepClickerStyle,
  tapeLineStyle,
  teamItemStyle,
  teamSliderStyle,
  viewportStyle,
  burgerButtonStyle,
  checkBoxStyle,
  maskTextFieldStyle,
  selectStyle,
  textFieldStyle,

  try3DModules
}

export const StylesContext = createContext(criticalStyles)

function MyApp({ Component, pageProps, router }) {
  return (
      <>
        <StylesContext.Provider value={criticalStyles}>
          <Provider store={store}>
            <AnimatePresence >
              {/*<motion.div exit={{}} initial={{}}>*/}
                <Component {...pageProps} key={router.route} />
              {/*</motion.div>*/}
            </AnimatePresence>
          </Provider>
        </StylesContext.Provider>
        <link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css" />

          <script type="text/javascript"
                  src="https://cdn.envybox.io/widget/cbk.js?cbk_code=356e33321275b385313506a15f65dacf" charSet="UTF-8"
                  async></script>

          <script type="text/javascript" dangerouslySetInnerHTML={{
            __html: `(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
              m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
              (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
  
              ym(83746909, "init", {
              clickmap:true,
              trackLinks:true,
              accurateTrackBounce:true,
              webvisor:true
            });`
          }}>

          </script>
          <noscript><div><img src="https://mc.yandex.ru/watch/83746909" style={{position:'absolute', left:'-9999px'}} alt="" /></div></noscript>
        </>
  )
}

export default MyApp
