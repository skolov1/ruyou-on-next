import { FC } from 'react'
import { Header } from '../../components/complexes/Header'
import { Footer } from '../../components/complexes/Footer'
import { SideSocial } from '../../components/complexes/SideSocial'
import { OrderStepsContainer } from '../../components/complexes/OrderStepsContainer'
import { motion } from 'framer-motion'
import { StylesContext } from '../_app'
import { useContext } from 'react'
import Head from "../../components/complexes/Head/Head";

type Props = {}

const Order: FC<Props> = () => {

    const { orderPageStyle } = useContext(StylesContext)

    return (
        <motion.div
            className={orderPageStyle.wrapper}
            animate={{ opacity: 1 }}
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
            transition={{ duration: .3 }}
        >
            <Head
                title={"Расскажите нам о вашем проекте | RuYou"}
                description={"Расскажите нам о вашем проекте и обязательно вам поможем и дадим профессиональную консультацию"}
            />
            <Header />
            <div className={orderPageStyle.content}>
                <OrderStepsContainer />

                <SideSocial />
            </div>
            <div className={orderPageStyle.footerWrap}>
                <Footer minimal />
            </div>

        </motion.div>


    )
}
export default Order