import { useRef, RefObject, useEffect  } from 'react'


type UseSelectorReturnType = {
   scrollRef: RefObject<HTMLDivElement>
}
 

 export const useScrollWatcher = (
 ): UseSelectorReturnType => {

    const scrollRef = useRef<HTMLDivElement>(null)

    const stopScrolling = (e) => { e.preventDefault() }
 
    useEffect(() => {
        if (scrollRef.current) {
            scrollRef.current.addEventListener('wheel', stopScrolling, { passive: false })
            scrollRef.current.addEventListener('touchmove', stopScrolling, { passive: false })
        }
        return () => {
        if (scrollRef.current) {
            scrollRef.current.removeEventListener('wheel', stopScrolling)
            scrollRef.current.removeEventListener('touchmove', stopScrolling)
        }
        }
    },[])
 
    return { scrollRef }
 }
 