import styles from './DevelopCounter.module.scss'
import { FC, ReactNode } from 'react'

type Props = {
  children: ReactNode
}

const DevelopCounter: FC<Props> = ({
  children
}) => {

  return (
    <div className={styles.wrapper}>
      {children}
    </div>
  )
}

export default DevelopCounter
