import styles from './SideSocial.module.scss'
import { FC } from 'react'

type Props = {}

const SideSocial: FC<Props> = ({
}) => {
  return (
    <div className={styles.linkSocialHolder}>
      <div className={styles.linkSocialItem}>
        <a href="https://www.instagram.com/ruyou_it/" target="_blank">insta</a>
      </div>
      {
        /*<div className={styles.linkSocialItem}>
          <a href="">vk</a>
        </div>*/
      }
    </div>
  )
}

export default SideSocial