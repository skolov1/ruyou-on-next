import styles from './OpenedCity.module.scss'
import { FC, useEffect, useState } from 'react'
import { YMaps, Map, Placemark } from 'react-yandex-maps'
import { Cities } from '../../../pages/contacts'
import { useScrollWatcher } from '../../hooks/scrollWatcher'

type Props = {
  onClose: () => void
  city: Cities
}

const OpenedCity: FC<Props> = ({
  onClose,
  city
}) => {
  const { scrollRef } = useScrollWatcher()
  const [opacity, setOpacity] = useState(0)

  useEffect(() => {
    setOpacity(1)
  }, [])

  return (
    <div
      className={styles.wrapper}
      ref={scrollRef}
      style={{ opacity: opacity }}
    >
      <div className={styles.cityWrapper}>

        <div
          className={styles.closeButton}
          onClick={onClose}
        />

        <div className={styles.mapHolder}>

          <div className={styles.cityInfoHolder}>
            <div className={styles.cityLabel}>{city.city}</div>
            <div className={styles.cityAddress}>{city.address}</div>
            <div className={styles.cityContacts}>
              <div className={styles.mail}>{city.mail}</div>
              <div className={styles.phone}>{city.phone}</div>
            </div>
          </div>

          <div className={styles.mapContainer}>
            <YMaps>
              <Map
                style={{ width: '100%', height: '100%' }}
                defaultState={city.mapCenter}
              >
                  <Placemark geometry={city.placemark} />
              </Map>
            </YMaps>
          </div>
        </div>

      </div>
    </div>
  )
}

export default OpenedCity