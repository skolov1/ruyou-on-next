import styles from './TeamSlider.module.scss'
import { FC } from 'react'
import { motion } from 'framer-motion'
import classNames from 'classnames'

type Props = {
}

const TeamSlider: FC<Props> = ({
}) => {

    return (
        <div className={styles.wrapper}>
                <div className={classNames(styles.slider,styles.animate)}>
                    <div className={styles.icon}>
                        <img src='/company/restIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/smartFeedIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/badenIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/teleplusIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/metroIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/restIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/smartFeedIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/badenIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/teleplusIcon.png'/>
                    </div>
                    <div className={styles.icon}>
                        <img src='/company/metroIcon.png'/>
                    </div>
                </div>
        </div>
    )
    }

export default TeamSlider