import styles from './QuoteBlock.module.scss'
import { FC, ReactNode } from 'react'
import classNames from 'classnames'

type Props = {
  colorTheme: 'black' | 'yellow'
  text: string
  mw: string
}

const QuoteBlock: FC<Props> = ({
  colorTheme,
  text,
  mw
}) => {
  return (
    <div className={classNames([styles.wrapper,
    { [styles.yellow]: colorTheme === 'yellow' },
    { [styles.black]: colorTheme === 'black' }
    ])}>
      <p className={styles.quotesText} style={{maxWidth:mw}}>
        {text}
      </p>
    </div>
  )
}

export default QuoteBlock
