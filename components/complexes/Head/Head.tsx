import React, {FC} from "react"
import Head from 'next/head'

type Props = {
    title?: string,
    description?: string,
}

const HeadMeta: FC<Props> = ({
    title = "RuYou",
    description = ""
                             }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <link rel="icon" href="/favicon.ico" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"></link>
    </Head>
  )
}

export default HeadMeta
