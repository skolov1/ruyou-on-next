import styles from './TeamItem.module.scss'
import { FC, ReactNode } from 'react'
import classNames from 'classnames'

type Props = {
  photo: string,
  profession: string
  name: string
}

const TeamItem: FC<Props> = ({
  photo,
  profession,
  name
}) => {
  return (
    <div className={styles.wrapper}>
      <img className={styles.image} src={photo} alt={name} />
      <div className={styles.nameHolder}>
        <div className={styles.name}>{name}</div>
        <div className={styles.workPlace}>{profession}</div>
      </div>
    </div>
  )
}

export default TeamItem
