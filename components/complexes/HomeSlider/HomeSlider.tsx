import styles from './HomeSlider.module.scss'
import {FC, useCallback, useRef, WheelEvent} from 'react'
import _ from 'lodash'
import MouseIcon from '../../../public/mouse-icon.svg'
import MoveIcon from '../../../public/move-slides-icon.svg'
import SlideItem from './SlideItem'
import { useSwipeable, } from 'react-swipeable'
import { Slides } from '../../../types'

type Props = {
  slidePosition: number
  onChange: (slide: number) => void
  slides: Slides[]
}

const HomeSlider: FC<Props> = ({
  slidePosition,
  onChange,
  slides
}) => {

  const onHandleSwipedRight = useCallback(() => {
    if (slidePosition > 1) {
      onChange(slidePosition - 1)
    }
  }, [slidePosition])

  const onHandleSwipedLeft = useCallback(() => {
    if (slidePosition < slides.length) {
      onChange(slidePosition + 1)
    }
  }, [slidePosition])

  const handlers = useSwipeable({
    onSwipedLeft: onHandleSwipedLeft,
    onSwipedRight: onHandleSwipedRight,
    preventDefaultTouchmoveEvent: true,
    trackMouse: true
  })

  const lastWheelAt = useRef(new Date())
  const scrollHandler = (e: WheelEvent<HTMLElement>) => {
    e.stopPropagation()
    const now = new Date()
    if (now.getTime() - lastWheelAt.current.getTime() < 30) {
      lastWheelAt.current = now
      return
    }

    lastWheelAt.current = now

    const delta = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? e.deltaX * -1 : e.deltaY

    if (delta > 0) {
      onHandleSwipedLeft()
    } else {
      onHandleSwipedRight()
    }
  }

  return (
    <section className={styles.wrapper} {...handlers} onWheel={scrollHandler}>
      <div
        className={styles.infoSlidesHolder}
      >
        {
          slides.map((s, key) =>
            <SlideItem
              isActive={slidePosition === key + 1}
              slide={s}
              key={key}
            />)
        }
        <div className={styles.mobileMoveIcon}>
          <MoveIcon />
        </div>
        <MouseIcon className={styles.mouseIcon} />
      </div>
    </section>
  )
}

export default HomeSlider
