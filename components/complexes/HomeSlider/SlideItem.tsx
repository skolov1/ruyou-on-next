// import YellowArrow from '../../../public/yellow-arrow.svg'
import styles from './HomeSlider.module.scss'
import { FC, useCallback } from 'react'
import { Slides } from '../../../types'
import Link from 'next/link'

type Props = {
    slide: Slides,
    isActive: boolean
}

const SliderItem: FC<Props> = ({
    slide,
    isActive
}) => {

    const prepareSlide = useCallback(() => {
        let stringWords: string[] = slide.title.split(' ')
        return {
            actionWord: stringWords.shift(),
            otherText: stringWords.join(' ')
        }
    }, [])

    let { actionWord, otherText } = prepareSlide()

    return (
        <div
            className={styles.infoSlideHolder}
            style={{ opacity: isActive ? 1 : 0, zIndex: isActive ? 2 : 1, pointerEvents: isActive ? 'initial' : 'none' }}
        >
            <div className={styles.textHolder}>
                <Link href={{
                    pathname: slide.link
                }}>
                    <a>
                        <span className={styles.infoGoldHeader}>
                            {actionWord}
                        </span>
                        {' '}{otherText}
                    </a>
                </Link>
            </div>
        </div>
    )
}

export default SliderItem



