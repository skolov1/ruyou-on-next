import styles from './ReviewClient.module.scss'
import { FC, ReactNode } from 'react'
import classNames from 'classnames'

type Props = {
  name: string,
  image: string,
  text: JSX.Element
}

const ReviewClient: FC<Props> = ({
  image, name, text
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.reviewUser}>
        <div
          className={styles.avatar}
          style={{ backgroundImage: `url(/projects/${image}` }}
        />
        <div>
          <div className={styles.name}>{name}</div>
          <div className={styles.secondName}>{name}</div>
        </div>
      </div>

      <div className={styles.paragraph}>
        {text}
      </div>


    </div>
  )
}

export default ReviewClient
