// import styles from './Try3D.module.scss'
import { FC, useContext } from 'react'
import ObjectItem from './ObjectItem'
import { StylesContext } from '../../../pages/_app'

type Props = {
  slide: number
}

// cache clean

const   Try3D: FC<Props> = ({
  slide
}) => {

  const { try3DModules } = useContext(StylesContext)

  return (
    <div
      className={try3DModules.wrapper}
    >
      <ObjectItem slide={slide} />
    </div>
  )
}

export default Try3D
