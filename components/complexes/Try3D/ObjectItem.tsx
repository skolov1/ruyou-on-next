// import try3DModules from './Try3D.module.scss'
import {
  FC,
  useCallback,
  useEffect,
  useRef,
  useState,
  useContext
} from 'react'
import Cube from '../../../public/cube-black.svg'
import Sphere from '../../../public/sphere-black.svg'
import Cone from '../../../public/cone-black.svg'
import Cylinder from '../../../public/cylinder-black.svg'
import { StylesContext } from '../../../pages/_app'

type Props = {
  slide: number
}

const ObjectItem: FC<Props> = ({
  slide
}) => {

  const refEl = useRef<HTMLDivElement>()
  const [trans, setTrans] = useState({ x: 0, y: 0, scale: 1 })
  const { try3DModules } = useContext(StylesContext)

  const onMouseMoveHandler = useCallback((e) => {
    if (refEl && refEl.current) {
      let rectData = refEl.current.getBoundingClientRect()

      let relX = e.pageX - rectData.left,
        relY = e.pageY - rectData.top

      let currentX = relX += rectData.width * -0.55,
        currentY = relY += rectData.height * -0.55

      let newX = currentX / 2500000,
        newY = currentY / 5000000

      setTrans({ x: newX, y: newY, scale: 1 })
    }
  }, [setTrans, refEl, trans])

  useEffect(() => {
    window.addEventListener('mousemove', onMouseMoveHandler)
    return () => {
      window.removeEventListener('mousemove', onMouseMoveHandler)
    }
  }, [])


  return (
    <div
      className={try3DModules.objectFeeller}
      ref={refEl}
      style={{ transform: `matrix3d(${trans.scale},0,0,${-trans.x},0,1,0,${-trans.y},0,0,1,0,0,0,0,1)` }}
    >
      {slide === 1 && <Cube />}
      {slide === 2 && <Sphere />}
      {slide === 3 && <Cone />}
      {slide === 4 && <Cylinder />}
      {slide === 5 && <Cube />}
      {slide === 6 && <Sphere />}
    </div>
  )
}

export default ObjectItem