import styles from './Viewport.module.scss'
import { FC, ReactNode } from 'react'
import classNames from 'classnames'

type Props = {
  children: ReactNode
  fullScreen?: boolean

}

const Viewport: FC<Props> = ({
  children,
  fullScreen
}) => {
  return (
    <section className={classNames(styles.wrapper, { [styles.fullScreen]: fullScreen })}>
      {children}
    </section>
  )
}

export default Viewport
