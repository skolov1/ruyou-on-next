import styles from './Header.module.scss'
import { BurgerButton } from '../../simples/BurgerButton'
import Link from 'next/link'
import Logo from '../../../public/Logo.svg'
import SupportIcon from '../../../public/support-icon.svg'
import PhoneIcon from '../../../public/phone-icon.svg'
import { FullScreenMenu } from '../FullScreenMenu'
import { FC } from 'react'
import { useAppSelector } from '../../hooks/store'
import { selectSideMenuStatus } from '../../../store/slices/site'
import { AnimatePresence } from 'framer-motion'


type Props = {}

const Header: FC<Props> = ({

}) => {

  const menuStatus = useAppSelector(selectSideMenuStatus)

  return (
    <header className={styles.wrapper}>

      <div className={styles.leftSide}>
        <BurgerButton />

        <div className={styles.logo}>
          <Link passHref href="/">
            <Logo className={styles.ruyou} />
          </Link>
        </div>

        <div className={styles.contacts}>
          <div className={styles.contactsItem}>
            <div className={styles.contactsItemIcon}>
              <SupportIcon />
            </div>
              <a href="mailto:support@ruyou.ru">support@ruyou.ru</a>
          </div>
          <div className={styles.contactsItem}>
            <div className={styles.contactsItemIcon}>
              <PhoneIcon />
            </div>
              <a href="tel:+74994441327">+7 (499) 444-13-27</a>
          </div>
        </div>
      </div>

      <div className={styles.mainMenu}>
        <ul className={styles.mainMenuList}>

          <Link href='/services/1'>
            <li className={styles.mainMenuListItem}>
              Услуги
            </li>
          </Link>

          <Link passHref href='/projects'>
            <li className={styles.mainMenuListItem}>
              Наши работы
            </li>
          </Link>

          <Link passHref href='/company'>
            <li className={styles.mainMenuListItem}>
              Компания
            </li>
          </Link>

          <Link passHref href='/order'>
            <li className={styles.mainMenuListItem}>
              Начать проект +
            </li>
          </Link>

        </ul>
      </div>
      <AnimatePresence >
        {menuStatus &&
          <FullScreenMenu />
        }
      </AnimatePresence>
    </header>
  )
}

export default Header