import styles from './StepClicker.module.scss'
import { FC } from 'react'
import classNames from 'classnames'

type Props = {
  onForward: () => void,
  onBack?: () => void,
    forwardDisabled?: boolean,
    loading?: boolean,
}

const StepClicker: FC<Props> = ({
  onForward,
  onBack,
                                    forwardDisabled = false,
                                    loading = false
}) => {
  return (
    <div className={styles.wrapper}>
      {onBack &&
        <div
          className={styles.back}
          onClick={() => onBack()}
        >Назад</div>
      }
      <div
        className={classNames(styles.forward, {
            [styles.disabled]: forwardDisabled,
            [styles.loading]: loading,
        })}
        onClick={() => onForward()}
      >
          <span className={styles.forwardContain}>Продолжить</span>
          <span className={styles.forwardLoader}>
                <span />
                <span />
                <span />
            </span>
      </div>
    </div>
  )
}

export default StepClicker
