import styles from './FactsCounter.module.scss'
import { FC, useCallback, useEffect, useRef, useState } from 'react'
import classNames from 'classnames'
import 'intersection-observer'
import { useIsVisible } from 'react-is-visible'

type Props = {
  topValue: number
  caption: string
}

const FactsCounter: FC<Props> = ({
  topValue,
  caption
}) => {

  const countersHolder = useRef()
  const isVisible = useIsVisible(countersHolder)

  const [wasVisible, setWasVisible] = useState(false)
  const [count, setCount] = useState(0)

  const interval = useRef<NodeJS.Timeout>()
  const counter = useRef(0)

  const timer = useCallback(() => {
    counter.current = counter.current + 1

    setCount(counter.current)

    if (counter.current === topValue) {
      clearInterval(interval.current)
    }
  }, [count, counter, setCount])

  useEffect(() => {
    if (isVisible && !wasVisible) {
      interval.current = setInterval(timer, 20)
      setWasVisible(true)
    }

  }, [isVisible, count, setCount, wasVisible, setWasVisible])

  return (
    <div className={styles.wrapper} ref={countersHolder}>
      <div className={styles.counter}>{count}</div>
      <div className={styles.caption}>{caption}</div>
    </div>
  )
}

export default FactsCounter
