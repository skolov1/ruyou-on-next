import styles from './ProjectImageSlider.module.scss'
import {
  FC,
  ReactNode,
  useCallback,
  useReducer,
  useRef,
  useState
} from 'react'
import classNames from 'classnames'
import HeaderProject from '../../../public/header-project-slider.svg'
import Slider from 'react-slick'

type Props = {
  slides: string[]
}

const ProjectImageSlider: FC<Props> = ({
  slides
}) => {

  const [current, setCurrent] = useState(0)
  const refSlider = useRef<any>()
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  }

  const onHandleBack = useCallback(() => {
    if (refSlider && refSlider.current) {
      refSlider.current.slickPrev()
    }
  }, [setCurrent, current])

  const onHandleForward = useCallback(() => {
    if (refSlider && refSlider.current) {
      refSlider.current.slickNext()
    }
  }, [setCurrent, current])

  return (
    <div className={styles.wrapper}>
      <div className={styles.sliderHeader}>
        <HeaderProject className={styles.headerProject} />
      </div>

      <div className={styles.screen}>

        <Slider
          {...settings}
          ref={refSlider}
        >
          {slides.map((s, i) =>
            <div
              key={i}
            >
              <img className={styles.screenImage} src={s} />
            </div>
          )}
        </Slider>

      </div>

      {slides.length > 1 && <div className={styles.controlHolder}>
        <div
          className={styles.controlsSteps}
          onClick={onHandleBack}
        >Назад</div>
        <div
          onClick={onHandleForward}
          className={styles.controlsSteps}
        >Вперёд</div>
      </div>}
    </div>
  )
}

export default ProjectImageSlider
