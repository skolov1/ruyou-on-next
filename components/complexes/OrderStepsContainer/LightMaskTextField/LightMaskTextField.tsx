import React, {
    useCallback,
    useEffect,
    useState,
    FocusEvent,
    forwardRef,
    useContext
} from 'react'
import _ from 'lodash'
import classNames from 'classnames'
// import lightMaskTextFieldStyle from './LightMaskTextField.module.scss'
import { StylesContext } from '../../../../pages/_app'
import InputMask, { Props as InputMaskProps } from 'react-input-mask'

export type Props = InputMaskProps & {
    id?: string
    label?: string
    helperText?: string
    error?: boolean
    classes?: {
        root?: string
        label?: string
        input?: string
    }
    outLabel?: boolean
    showRequired?: boolean
}

/**
 * Компонент Input
 * @param {string|undefined} id - идентификатор
 * @param {string} label - Название текстового поля
 * @param value - Значение текстового поля
 * @param {string} helperText - Текст подсказки под текстовым полем
 * @param {boolean} error - Подсветка ошибки
 * @param {boolean} multiline - Если true - выводиться textarea
 * @param classes - Дополнительные стилевые классы
 * @param onChange - Callback изменения текстового поля
 * @param onFocus - Callback фокуса
 * @param onBlur - Callback потери фокуса
 * @param rest
 * @constructor
 */

// Пример формирования маски из нескольких параметров

// const firstLetter = /(?!.*[DFIOQU])[A-VXY]/i;
// const letter = /(?!.*[DFIOQU])[A-Z]/i;
// const digit = /[0-9]/;
// const mask = [firstLetter, digit, letter, " ", digit, letter, digit];
// 9	0-9         - маска для вывода цифр
// a	a-z, A-Z    - маска для вывода букв
// *	0-9, a-z, A-Z  - маска для вывода букв и цифр

const LightMaskTextField = forwardRef<HTMLInputElement, Props>(
    (
        {
            id: iid,
            label,
            value,
            helperText,
            error = false,
            classes = {},
            onChange,
            onFocus,
            onBlur,
            disabled,
            outLabel = false,
            showRequired = false,
            ...rest
        },
        ref
    ) => {
        const [id, setId] = useState('')
        const [focus, setFocus] = useState(false)

        const { lightMaskTextFieldStyle } = useContext(StylesContext)

        // Получение идентификатора
        // Если идентификатор отсутствует, генерируется случайная строка
        useEffect(() => {
            const id = iid || _.uniqueId('text-field')
            setId(id)
        }, [iid, setId])

        // Обработчик фокуса
        // При фокусировки добавляется класс, который меняет местоположение label
        const onHandleFocus = useCallback(
            (event: FocusEvent<HTMLInputElement>) => {
                setFocus(true)
                if (onFocus) {
                    onFocus(event)
                }
            },
            [setFocus, onFocus]
        )

        // Обработка потери фокуса
        // Удаляется класс, который устанавливался при фокусировке
        const onHandleBlur = useCallback(
            (event: FocusEvent<HTMLInputElement>) => {
                setFocus(false)
                if (onBlur) {
                    onBlur(event)
                }
            },
            [onBlur]
        )

        // Вывод компонента
        return (
            <div
                className={classNames(lightMaskTextFieldStyle.root, classes.root, {
                    [lightMaskTextFieldStyle.focus]: focus,
                    [lightMaskTextFieldStyle.filled]: !!value,
                    [lightMaskTextFieldStyle.error]: error,
                    [lightMaskTextFieldStyle.disabled]: !!disabled,
                })}
            >
                {outLabel && label && (
                    <div className={lightMaskTextFieldStyle.outLabel}>
                        {label}
                        {showRequired && (
                            <>
                                {' '}
                                <span className={lightMaskTextFieldStyle.requiredIndicator}>
                                    *
                                </span>
                            </>
                        )}
                    </div>
                )}

                <div className={lightMaskTextFieldStyle.base}>
                    {label && !outLabel && (
                        <label
                            className={classNames(lightMaskTextFieldStyle.label, classes.label)}
                            htmlFor={id}
                        >
                            {label}
                            {showRequired && (
                                <>
                                    {' '}
                                    <span className={lightMaskTextFieldStyle.requiredIndicator}>
                                        *
                                    </span>
                                </>
                            )}
                        </label>
                    )}

                    <InputMask
                        {...rest}
                        className={classNames(lightMaskTextFieldStyle.input, classes.input)}
                        id={id}
                        value={value || ''}
                        onFocus={onHandleFocus}
                        onBlur={onHandleBlur}
                        onChange={onChange}
                        disabled={disabled}
                        inputRef={ref}
                    />

                    {label && !outLabel && (
                        <div className={lightMaskTextFieldStyle.notch}>
                            <span className={lightMaskTextFieldStyle.notchContainer}>
                                {label}
                            </span>
                        </div>
                    )}
                </div>

                {helperText && <p className={lightMaskTextFieldStyle.helperText}>{helperText}</p>}
            </div>
        )
    }
)

export default LightMaskTextField
