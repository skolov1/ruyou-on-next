import React, { FC, useState, useEffect, useCallback, useRef, useContext } from 'react'
import classNames from 'classnames'
// import LightSelectStyle from './LightSelect.module.scss'
import { ReactComponent as ArrowIcon } from '../../../../public/arrow-dropdown-ico.svg'
import { StylesContext } from '../../../../pages/_app'

/**
 * Вычисление выбранного значения в селекторе.
 * В случае, если значение установлено, то выводим его, в противном случае undefined
 * @param {any} value - Значение селектора
 * @param {Array<OptionsItem>} options - Набор возможных опций для выбора в селекторе
 */
const getSelectContents = (value: any, options: Array<OptionsItem>):
    | undefined
    | OptionsItem => {
    if (value === undefined || value === null || value === '') {
        return undefined
    } else {
        return options.find((x) => x.value === value)
    }
}

// Тип для одной возможной опции в селекторе
export type OptionsItem = {
    value: any
    label?: string | JSX.Element
}

export type SelectOptions = {
    options: Array<OptionsItem>
    label?: string
    value?: any
    placeholder?: string
    onChange?: (value: any, name?: string) => void
    name?: string
    helperText?: string
    error?: boolean
    classes?: {
        selectWrapper?: string
        textInside?: string
        arrow?: string
        optionList?: string
        option?: string
        label?: string
        placeholder?: string
        outLabel?: string
    }
    disabled?: boolean
    outLabel?: boolean
}

export type Props = SelectOptions & {}

/**
 * Вывод селектора
 * @param {Object} (optional) classes - Стили, применяемы к селектору помимо стандартных
 * @param {string} (optional) label - Вспомогательная метка, всплывающая вверх при взаимодействии с селектором
 * @param {any} (optional) value - Текущее значение селектора
 * @param {Array<OptionsItem>} options - Набор возможных для выбора в селекторе опций
 * @param {boolean} (optional) disabled - Свойство для блокировки селектора
 * @param {string} (optional) placeholder - Заполнитель селектора, исчезающий при установленном значении
 * @param {string} (optional) helperText - Текст подсказки под селектором
 * @param {boolean} (optional) error - Подсветка селектора в случае ошибки
 * @param onChange (optional) - Колбэк для извлечения значения селектора при клике на определенную опцию
 * @param {string} (optional) name - Имя селектора
 * @param {string} (optional) transNS - Параметр необходимый для перевода (Пространство имен)
 * @param {i18nType} (optional) i18n - Параметр необходимый для перевода
 * @constructor
 */
const LightSelect: FC<Props> = ({
    classes = {},
    label,
    value,
    options,
    disabled,
    placeholder,
    helperText,
    error = false,
    onChange,
    name,
    outLabel = false,
}) => {
    // Подключение функции перевода

    const optionsBlock = useRef<HTMLDivElement>(null)

    const { LightSelectStyle } = useContext(StylesContext)

    const [openStatus, setOpenStatus] = useState(false)

    // Вычисление текущего значения для отображения текста выбранного значения в селекторе.
    const currentOption: OptionsItem | undefined = getSelectContents(
        value,
        options
    )

    // Метод сброса фокуса с выбранного элемента
    const loseFocus = useCallback(() => {
        if (optionsBlock && optionsBlock.current) {
            optionsBlock.current.blur()
        }
    }, [])

    // Метод закрытия селектора при клике вне самого селектора
    const closeSelectOutOfBlock = useCallback(
        (event: any) => {
            if (optionsBlock && optionsBlock.current) {
                // Проверка добавлена для устранения бага в Firefox
                if (event.path && !event.path.includes(optionsBlock.current)) {
                    setOpenStatus(false)
                    loseFocus()
                }
            }
        },
        [loseFocus]
    )

    // Обновление значения селектора при клике на определенную опцию
    const onHandleItemClick = useCallback(
        (value: string | number) => {
            if (onChange) {
                onChange(value, name)
            }
            setOpenStatus(false)
            loseFocus()
        },
        [loseFocus, name, onChange]
    )

    // Раскрытие пунктов меню с опциями при фокусе на селекторе
    const onHandleFocus = useCallback(() => {
        if (!disabled) setOpenStatus(true)
    }, [disabled])

    // Закрытие пунктов меню с опциями при потере фокуса с селектора
    const onHandleBlur = useCallback(() => {
        if (!disabled) setOpenStatus(false)
    }, [disabled])

    // Установка/удаление обработчика события на документе.
    useEffect(() => {
        document.addEventListener('click', closeSelectOutOfBlock, false)
        return () => {
            document.removeEventListener('click', closeSelectOutOfBlock, false)
        }
    }, [closeSelectOutOfBlock])

    // Вывод компонента
    return (
        <div className={LightSelectStyle.root}>
            {outLabel && label && (
                <div className={classNames(LightSelectStyle.outLabel, classes.outLabel)}>
                    {label}
                </div>
            )}

            <div
                ref={optionsBlock}
                onFocus={onHandleFocus}
                onBlur={onHandleBlur}
                className={classNames(LightSelectStyle.selectWrap, {
                    [LightSelectStyle.error]: error,
                    [LightSelectStyle.opened]:
                        openStatus && options && options.length !== 0,
                    [LightSelectStyle.filled]: value || value === 0,
                    [LightSelectStyle.disabled]: disabled,
                })}
                tabIndex={0}
            >
                <ArrowIcon
                    className={classNames(LightSelectStyle.arrow, classes.arrow)}
                />
                <div
                    className={classNames(
                        LightSelectStyle.valueHolder,
                        classes.selectWrapper,
                        {
                            [LightSelectStyle.filled]: value || value === 0,
                        }
                    )}
                >
                    {placeholder && !currentOption && (
                        <div
                            className={classNames(
                                LightSelectStyle.placeholder,
                                classes.placeholder
                            )}
                        >
                            {placeholder}
                        </div>
                    )}
                    {label && !outLabel && (
                        <>
                            <label
                                className={classNames(
                                    LightSelectStyle.label,
                                    classes.label,
                                    {
                                        [LightSelectStyle.labelVisible]:
                                            !value && !placeholder,
                                    }
                                )}
                            >
                                {label}
                            </label>
                            <div className={LightSelectStyle.notch}>
                                <span className={LightSelectStyle.notchContainer}>
                                    {label}
                                </span>
                            </div>
                        </>
                    )}
                    {!label &&
                        !placeholder &&
                        !value &&
                        value !== 0 &&
                        'Выберите'}
                    <p
                        className={classNames(
                            LightSelectStyle.someText,
                            classes.textInside
                        )}
                    >
                        {currentOption && currentOption.label}
                    </p>
                </div>
                {helperText && (
                    <p className={LightSelectStyle.helperText}>{helperText}</p>
                )}
                {openStatus && options.length !== 0 && (
                    <ul
                        className={classNames(
                            LightSelectStyle.selectList,
                            classes.optionList
                        )}
                    >
                        {options.map((option, index) => (
                            <li
                                className={classes.option}
                                onClick={() => {
                                    onHandleItemClick(option.value)
                                }}
                                key={index}
                            >
                                {option.value || option.value === 0
                                    ? option.label : 'Не выбрано'}
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
}

export default LightSelect
