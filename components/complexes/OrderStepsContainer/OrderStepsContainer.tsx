import {
  FC,
  useCallback,
  useReducer,
  useState,
  useContext
} from 'react'

import { Order } from '../../../types'
import { StylesContext } from '../../../pages/_app'
import {FirstStep, SecondStep, ThirdStep, ForthStep, Finish, KindJob, KindActivity, ServiceOptions} from './Steps'
import {orderStepsContainerUrl} from "../../../constants";

type Props = {}

export const UPDATE_FIRST_STEP = 'UPDATE_FIRST_STEP'
export const UPDATE_SECOND_STEP = 'UPDATE_SECOND_STEP'
export const UPDATE_THIRD_STEP = 'UPDATE_THIRD_STEP'
export const UPDATE_FORTH_STEP = 'UPDATE_FORTH_STEP'

const initState: Order = {
  acceptedFiles: [],
  doing: undefined,
  mail: '',
  name: '',
  phone: '',
  service: undefined,
  youAre: undefined
}

const reducer = (state: Order, action): Order => {
  switch (action.type) {
    case UPDATE_FIRST_STEP:
      return {
        ...state,
        doing: action.payload.doing,
        youAre: action.payload.youAre
      }
    case UPDATE_SECOND_STEP:
      return {
        ...state,
        service: action.payload.service,
      }
    case UPDATE_THIRD_STEP:
      return {
        ...state,
        name: action.payload.name,
        mail: action.payload.mail,
        phone: action.payload.phone
      }
    case UPDATE_FORTH_STEP:
      return {
        ...state,
        acceptedFiles: [
          ...state.acceptedFiles,
          ...action.payload.acceptedFiles
        ]
      }
    default:
      return state
  }
}

const OrderStepsContainer: FC<Props> = ({
}) => {

  const [steps, setSteps] = useState(1)
  const [state, dispatch] = useReducer(reducer, initState)
  const [loading, setLoading] = useState(false)

  const { orderStepsContainerStyle } = useContext(StylesContext)

  const onHandleForward = useCallback((data: {
    type: string,
    action: { payload: Order }
  }) => {
    dispatch({
      type: data.type,
      payload: data.action.payload
    })
    if (steps < 4) {
      setSteps(steps + 1)
    } else {

      // Отправляем данные
      setLoading(true)

      const actualState = data.action.payload

      // @ts-ignore
      ym(83746909,'reachGoal','Forma_footer')

      const form = new FormData()

      const findYouAre = KindJob.find(kj => kj.value === actualState.youAre)
      if (!findYouAre) {
        console.error('youAre not found')
        return
      }
      const findDoing = KindActivity.find(ka => ka.value === actualState.doing)
      if (!findDoing) {
        console.error('doing not found')
        return
      }
      const findService = ServiceOptions.find(s => s.value === actualState.service)
      if (!findService) {
        console.error('service not found')
        return
      }


      form.append('youAre', findYouAre.label as 'string')
      form.append('doing', findDoing.label as 'string')
      form.append('service', findService.label as 'string')
      form.append('name', actualState.name)
      form.append('phone', actualState.phone)
      form.append('email', actualState.mail)

      actualState.acceptedFiles.forEach(file => {
        form.append('files[]', file)
      })

      fetch(orderStepsContainerUrl, {
        method: 'POST',
        body: form
      }).then(response => {
        return response.json()
      }).then(() => {
        setLoading(false)
        setSteps(5)
      })
    }
  }, [steps, setSteps])

  const onHandleBack = useCallback(() => {
    if (steps > 1) {
      setSteps(steps - 1)
    }
  }, [steps, setSteps])

  return (
    <div className={orderStepsContainerStyle.wrapper}>
      <div className={orderStepsContainerStyle.formHolder}>
        {steps === 1 ?
          <FirstStep
            onHandleContinue={onHandleForward}
            state={state}
          /> : steps === 2 ?
          <SecondStep
            onHandleContinue={onHandleForward}
            onHandleBack={onHandleBack}
            state={state}
          /> : steps === 3 ?
          <ThirdStep
            onHandleContinue={onHandleForward}
            onHandleBack={onHandleBack}
            state={state}
          /> : steps === 4 ?
          <ForthStep
            state={state}
            onHandleContinue={onHandleForward}
            onHandleBack={onHandleBack}
            loading={loading}
          /> : steps === 5 ?
          <Finish />
          : null
        }
      </div>
    </div>
  )
}

export default OrderStepsContainer
