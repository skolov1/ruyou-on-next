
import { FC, useRef, useCallback, useContext, ChangeEvent } from 'react'
import CheckMark from '../../../../public/check-mark.svg'
import PlusIcon from '../../../../public/plus-icon.svg'
import { StylesContext } from '../../../../pages/_app'

type Props = {
    onChange: (files: File[]) => void
    files: File[]
}

const LightDropFiles: FC<Props> = ({
    onChange,
    files
}) => {

    const { lightDropFilesStyle } = useContext(StylesContext)

    const fileInputRef = useRef(null);

    const onTargetClick = () => {
        fileInputRef.current.click()
    }

    const onFileInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { files: uploadedFiles } = event.target;
        let newFiles = [ ...files ]

        for (let i = 0; i < uploadedFiles.length; i++) {
            newFiles.push(uploadedFiles[i])
        }

        onChange(newFiles)
    }

    return (
        <>
            <div className={lightDropFilesStyle.wrapper}>
                <span
                    onClick={onTargetClick}
                    className={lightDropFilesStyle.openDialogWindowLabel}
                >
                    Прикрепите файл
                    <PlusIcon className={lightDropFilesStyle.plusIcon} />
                </span>

                <input
                    onChange={onFileInputChange}
                    ref={fileInputRef}
                    type="file"
                    className={lightDropFilesStyle.hiddenInput}
                    multiple
                />
            </div>


            {files.length > 0 &&
                <div className={lightDropFilesStyle.uploadedFilesList}>
                    {files.map((f, key) => (
                        <div
                            className={lightDropFilesStyle.uploadedFile}
                            key={key}
                        >
                            {f.name}
                            {/*<div className={lightDropFilesStyle.checkMarkHolder}>
                                <CheckMark />
                            </div>*/}
                        </div>
                    ))}
                </div>
            }
        </>
    )
}

export default LightDropFiles
