import React, {FC, useContext, useEffect} from "react";
import {useRouter} from "next/router";
import {StylesContext} from "../../../../pages/_app";

const Finish: FC = () => {
    const router = useRouter()

    const { orderStepsContainerStyle } = useContext(StylesContext)

    useEffect(() => {
        setTimeout(() => {
            router.push('/').then()
        }, 3000)
    }, [])

    return (
        <div className={orderStepsContainerStyle.finish}>
            Спасибо за заявку. <span className={orderStepsContainerStyle.finishSmilesGroup}>🎉😃👍</span><br /> Мы свяжемся с вами в ближайшее время! 😘
        </div>
    )
}

export default Finish