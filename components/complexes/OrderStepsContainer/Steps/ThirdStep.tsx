import { FC, useState, useCallback, useContext, ChangeEvent } from 'react'
import {validateEmail as validateEmailLib, validatePhone as validataPhoneLib} from "../../../../utils/string";
import { LightMaskTextField } from '../LightMaskTextField'
import { LightTextField } from '../LightTextField'
import { StepClicker } from '../../StepClicker'
import { Order } from '../../../../types'
import { UPDATE_THIRD_STEP } from '../OrderStepsContainer'
import { emailRE, numberRE } from '../../../../constants'
import { StylesContext } from '../../../../pages/_app'


const validateName = (value: string) => {
  if (!value) {
    return 'Укажите свое имя'
  } else if (numberRE.test(value)) {
    return 'Некорректное имя'
  }
  return ''
}

const validatePhone = (value: string) => {
  if (!value) {
    return 'Укажите свой телефон'
  } else if (!validataPhoneLib(value)) {
    return 'Некорректный телефон'
  }

  return ''
}

const validateEmail = (value: string) => {
  if (!value) {
    return ''
  } else if (!validateEmailLib(value)) {
    return 'Некорректная почта'
  }

  return ''
}



type Props = {
  onHandleContinue: (data: {
    type: string,
    action: { payload: Order }
  }) => void
  onHandleBack: () => void,
  state: Order
}

const ThirdStep: FC<Props> = ({
  onHandleContinue,
  onHandleBack,
  state
}) => {

  const { orderPageStyle, orderStepsContainerStyle } = useContext(StylesContext)

  const [name, setName] = useState<string>(state.name)
  const [mail, setMail] = useState<string>(state.mail)
  const [phone, setPhone] = useState<string>(state.phone)

  const [nameError, setNameError] = useState<string>(validateName(state.name))
  const [mailError, setMailError] = useState<string>(validateEmail(state.mail))
  const [phoneError, setPhoneError] = useState<string>(validatePhone(state.phone))

  const [touchedName, setTouchedName] = useState(false)
  const [touchedMail, setTouchedMail] = useState(false)
  const [touchedPhone, setTouchedPhone] = useState(false)

  const onHandleForward = () => {

    setTouchedName(true)
    setTouchedPhone(true)
    setTouchedMail(true)

    if (nameError || mailError || phoneError) {
      return
    }

    onHandleContinue({
      type: UPDATE_THIRD_STEP,
      action: {
        payload: {
          ...state,
          name,
          mail,
          phone
        }
      }
    })

  }

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    let { value } = event.target
    setName(value)
    setNameError(validateName(value))
  }
  const handleNameBlur = () => {
    setTouchedName(true)
    setNameError(validateName(name))
  }

  const handlePhoneChange = (event: ChangeEvent<HTMLInputElement>) => {
    let { value } = event.target
    setPhone(value)
    setPhoneError(validatePhone(value))
  }
  const handlePhoneBlur = () => {
    setTouchedPhone(true)
    setPhoneError(validatePhone(phone))
  }

  const handleMailChange = (event: ChangeEvent<HTMLInputElement>) => {
    let { value } = event.target
    setMail(value)
    setMailError(validateEmail(value))
  }
  const handleMailBlur = () => {
    setTouchedMail(true)
    setMailError(validateEmail(mail))
  }

  return (
    <div className={orderPageStyle.stepHolder}>
      <div className={orderStepsContainerStyle.stepContainer}>

        <div className={orderStepsContainerStyle.formGroup}>
          <div className={orderStepsContainerStyle.formGroupLabel}>
            Ваше имя *
          </div>
          <div className={orderStepsContainerStyle.inputHolder}>
            <LightTextField
              label={'Введите имя'}
              onChange={handleNameChange}
              onBlur={handleNameBlur}
              value={name}
              error={touchedName && !!nameError}
              helperText={touchedName && !!nameError ? nameError : ''}
            />
          </div>
        </div>

        <div className={orderStepsContainerStyle.formGroup}>
          <div className={orderStepsContainerStyle.formGroupLabel}>
            Ваш номер *
          </div>
          <div className={orderStepsContainerStyle.inputHolder}>
            <LightMaskTextField
              label='Ваш номер'
              value={phone}
              id='my-input-id'
              name="phone"
              mask="+7 (999) 999 99 99"
              onChange={handlePhoneChange}
              onBlur={handlePhoneBlur}
              error={touchedPhone && !!phoneError}
              helperText={touchedPhone && !!phoneError ? phoneError : ''}
            />
          </div>
        </div>

        <div className={orderStepsContainerStyle.formGroup}>
          <div className={orderStepsContainerStyle.formGroupLabel}>
            и почта
          </div>
          <div className={orderStepsContainerStyle.inputHolder}>
            <LightTextField
              label={'Введите E-mail'}
              onChange={handleMailChange}
              onBlur={handleMailBlur}
              value={mail}
              error={touchedMail && !!mailError}
              helperText={touchedMail && !!mailError ? mailError : ''}
              type="email"
            />
          </div>
        </div>

      </div>

      <div className={orderStepsContainerStyle.stepClickerHolder}>
        <StepClicker
          onForward={onHandleForward}
          onBack={onHandleBack}
          forwardDisabled={!!nameError || !!phoneError || !!mailError}
        />
      </div>

    </div>
  )
}

export default ThirdStep
