import { FC, useState, useCallback, useContext } from 'react'
// import orderPageStyle from '../OrderStepsContainer.module.scss'
import { StepClicker } from '../../StepClicker'
import { Order, Files } from '../../../../types'
import { UPDATE_FORTH_STEP } from '../OrderStepsContainer'
import { StylesContext } from '../../../../pages/_app'
import { LightDropFiles } from './../LightDropFiles'

type Props = {
  onHandleContinue: (data: {
    type: string,
    action: { payload: Order }
  }) => void
  onHandleBack: () => void,
  state: Order,
  loading: boolean,
}

const ForthStep: FC<Props> = ({
  onHandleContinue,
  onHandleBack,
  state,
    loading,
}) => {

  const { orderPageStyle, orderStepsContainerStyle } = useContext(StylesContext)
  const [files, setFiles] = useState<File[]>(state.acceptedFiles)

  const handleForwardClick = () => {
    onHandleContinue({
      type: UPDATE_FORTH_STEP,
      action: {
        payload: {
          ...state,
          acceptedFiles: files,
        }
      }
    })
  }

  return (
    <div className={orderPageStyle.stepHolder}>
      <div className={orderStepsContainerStyle.stepLabel}>Файлы JPG, PNG, PDF</div>

      <div className={orderStepsContainerStyle.stepContainer}>
        <div className={orderStepsContainerStyle.dropFilesHolder}>
          <LightDropFiles
            files={files}
            onChange={setFiles}
          />
        </div>
      </div>

      <div className={orderStepsContainerStyle.stepClickerHolder}>
        <StepClicker
          onForward={handleForwardClick}
          onBack={onHandleBack}
          loading={loading}
        />
      </div>

    </div>
  )
}

export default ForthStep
