import { FC, useState, useCallback, useContext } from 'react'
import { LightSelect, OptionsItem } from '../LightSelect'
// import orderPageStyle from '../OrderStepsContainer.module.scss'
import { StepClicker } from '../../StepClicker'
import { Order } from '../../../../types'
import { UPDATE_SECOND_STEP } from '../OrderStepsContainer'
import { StylesContext } from '../../../../pages/_app'

export const KindJob: OptionsItem[] = [
  { value: 1, label: 'Мобильная разработка' },
  { value: 2, label: 'Web-сервис' },
  { value: 3, label: 'Интеграции' },
  { value: 4, label: 'eCommerce' },
  { value: 5, label: 'StartUp' },
]

type Props = {
  onHandleContinue: (data: {
    type: string,
    action: { payload: Order }
  }) => void
  onHandleBack: () => void,
  state: Order
}

const SecondStep: FC<Props> = ({
  onHandleContinue,
  onHandleBack,
  state
}) => {

  const { orderPageStyle, orderStepsContainerStyle } = useContext(StylesContext)

  const [service, setService] = useState<number>(state.service)

  const onHandleForward = () => {
    if (!service) {
      return
    }

    onHandleContinue({
      type: UPDATE_SECOND_STEP,
      action: {
        payload: {
          ...state,
          service,
        }
      }
    })

  }


  return (
    <div className={orderPageStyle.stepHolder}>
      <div className={orderStepsContainerStyle.stepContainer}>
        <div className={orderStepsContainerStyle.formGroup}>
          <div className={orderStepsContainerStyle.formGroupLabel}>
            Выберите услугу *
          </div>
          <div className={orderStepsContainerStyle.inputHolder}>
            <LightSelect
                options={KindJob}
                label={'Выберите услугу'}
                onChange={setService}
                value={service}
            />
          </div>
        </div>

      </div>

      <div className={orderStepsContainerStyle.stepClickerHolder}>
        <StepClicker
          onForward={onHandleForward}
          onBack={onHandleBack}
          forwardDisabled={!service}
        />
      </div>

    </div>
  )
}

export default SecondStep
