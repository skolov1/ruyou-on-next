import { FC, useState, useCallback, useContext } from 'react'
import { LightSelect, OptionsItem } from '../LightSelect'
import { StepClicker } from '../../StepClicker'
import { Order } from '../../../../types'
import { UPDATE_FIRST_STEP } from '../OrderStepsContainer'
import { StylesContext } from '../../../../pages/_app'

export const KindJob: OptionsItem[] = [
  { value: 1, label: 'Руководитель компании' },
  { value: 2, label: 'Руководитель проекта' },
  { value: 3, label: 'ИТ-специалист' },
  { value: 4, label: 'Другое' },
]

export const KindActivity: OptionsItem[] = [
  { value: 1, label: 'Ритейл' },
  { value: 2, label: 'Производство' },
  { value: 3, label: 'ИТ' },
  { value: 4, label: 'Startup' },
  { value: 5, label: 'Другое' },
]

type Props = {
  onHandleContinue: (data: {
    type: string,
    action: { payload: Order }
  }) => void
  state: Order
}

const FirstStep: FC<Props> = ({
  onHandleContinue,
  state
}) => {

  const [youAre, setYouAre] = useState<number>(state.youAre)
  const [doing, setDoing] = useState<number>(state.doing)

  const { orderPageStyle, orderStepsContainerStyle } = useContext(StylesContext)

  const onHandleForward = () => {
    if (!doing || !youAre) {
      return
    }

    onHandleContinue({
      type: UPDATE_FIRST_STEP,
      action: {
        payload: {
          ...state,
          doing,
          youAre
        }
      }
    })

  }

  return (
    <div className={orderPageStyle.stepHolder}>
      <div className={orderStepsContainerStyle.stepLabel}>Начать</div>

      <div className={orderStepsContainerStyle.stepContainer}>
        <div className={orderStepsContainerStyle.formGroup}>
          <div className={orderStepsContainerStyle.formGroupLabel}>
            Вы, являетесь *
          </div>
          <div className={orderStepsContainerStyle.inputHolder}>
            <LightSelect
                options={KindJob}
                label={'Выберите кем вы являетесь'}
                onChange={setYouAre}
                value={youAre}
            />
          </div>
        </div>

        <div className={orderStepsContainerStyle.formGroup}>
          <div className={orderStepsContainerStyle.formGroupLabel}>
            что вы делаете *
          </div>
          <div className={orderStepsContainerStyle.inputHolder}>
            <LightSelect
              options={KindActivity}
              label={'Выберите вашу деятельность'}
              onChange={setDoing}
              value={doing}
            />
          </div>
        </div>
      </div>

      <div className={orderStepsContainerStyle.stepClickerHolder}>
        <StepClicker
          onForward={onHandleForward}
          forwardDisabled={!youAre || !doing}
        />
      </div>

    </div>
  )
}

export default FirstStep
