import styles from './MainSlide.module.scss'
import { FC, ReactNode } from 'react'
import { Viewport } from '../Viewport'


type Props = {
}

const MainSlide: FC<Props> = ({ }) => {
  return (
    <Viewport fullScreen>
      <div className={styles.wrapper}>
        <p className={styles.goldTitle}>
          Кто мы?
        </p>
        <div className={styles.mainTitle}>
          Компания небольшое <br /> описание
        </div>
      </div>
    </Viewport>
  )
}

export default MainSlide
