import styles from './FullScreenMenu.module.scss'
import { FC, useCallback, useEffect, useState } from 'react'
import Link from 'next/link'
import { useAppDispatch } from '../../hooks/store'
import { siteSlice } from '../../../store/slices/site'
import { useScrollWatcher } from '../../hooks/scrollWatcher'
import { motion } from 'framer-motion'

type Props = {}

const FullScreenMenu: FC<Props> = ({
}) => {
  const { scrollRef } = useScrollWatcher()
  const dispatch = useAppDispatch()
  const { changeMenuStatus } = siteSlice.actions

  const onHandleCloseMenu = useCallback(() => {
    dispatch(changeMenuStatus(false))
  }, [])

  return (
    <motion.div
      className={styles.wrapper}
      initial={{ marginTop: '-50%' }}
      animate={{ transition : {duration: 1}, marginTop: '0px' }}
      exit={{ marginTop: '-50%' }}
      transition={{ duration: 1 }}
      ref={scrollRef}
    >
      <div
        className={styles.closeButton}
        onClick={onHandleCloseMenu}
      />

      <div className={styles.menuItemWrapper}>

        <ul className={styles.menuItem}>
          <li
            className={styles.menuItemListItem}
            onClick={onHandleCloseMenu}
          >
            <Link href='/services/1'>
              <a className={styles.menuLink}>Услуги</a>
            </Link>
          </li>

          <li
            className={styles.menuItemListItem}
            onClick={onHandleCloseMenu}
          >
            <Link href='/projects'>
              <a className={styles.menuLink}>Портфолио</a>
            </Link>
          </li>

          <li
            className={styles.menuItemListItem}
            onClick={onHandleCloseMenu}
          >
            <Link href='/company'>
              <a className={styles.menuLink}>Компания</a>
            </Link>
          </li>

          <li
            className={styles.menuItemListItem}
            onClick={onHandleCloseMenu}
          >
            <Link href='/contacts'>
              <a className={styles.menuLink}>Контакты</a>
            </Link>
          </li>
        </ul>

        <div className={styles.menuItem}>
          <div className={styles.menuInfoItem}>
            <a href='mailto:support@ruyou.ru'></a>support@ruyou.ru</div>
          <div className={styles.menuInfoItem}>
            <a href='tel:+74994441327'>+7 (499) 444-13-27</a>
          </div>
          <Link href='/order'>
            <div
              onClick={onHandleCloseMenu}
              className={styles.makeOrder}
            >Оставить заявку</div>
          </Link>
        </div>
      </div>
    </motion.div>
  )
}

export default FullScreenMenu
