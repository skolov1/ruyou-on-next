import styles from './Modal.module.scss'
import { FC, ReactNode } from 'react'

type Props = {
  children: ReactNode
}

const Modal: FC<Props> = ({
  children,
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.contentHolder}>
        {children}
      </div>
    </div>
  )
}

export default Modal
