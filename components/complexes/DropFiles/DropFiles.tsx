import styles from './DropFiles.module.scss'
import { FC, useState, useRef, useCallback, ChangeEvent } from 'react'
import classNames from 'classnames'
import { FileDrop } from 'react-file-drop'
import DropCloud from '../../../public/upload-icon.svg'
import CheckMark from '../../../public/check-mark.svg'

type Props = {
    onChange: (files: File[]) => void
    files: File[]
}

const DropFiles: FC<Props> = ({
    onChange,
    files
}) => {

    const [enter, setEnter] = useState(false)
    const fileInputRef = useRef(null);

    const onTargetClick = () => {
        fileInputRef.current.click()
    }

    const onHandleDropFiles = useCallback((e) => {
        let newFiles = []
        if (files.length > 0) {
            newFiles = [
                ...files,
                ...e
            ]
        } else {
            newFiles.push(...e)
        }

        onChange(newFiles)
    }, [files, onChange])

    const onFileInputChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        const { files: uploadedFiles } = event.target;
        let newFiles = [ ...files ]

        for (let i = 0; i < uploadedFiles.length; i++) {
            newFiles.push(uploadedFiles[i])
        }

        onChange(newFiles)
    }, [files, onChange])

    return (
        <>
            <div
                className={styles.wrapper}
            >
                <FileDrop
                    className={styles.fileDropper}
                    onDrop={onHandleDropFiles}
                    onDragLeave={() => setEnter(false)}
                    onDragOver={() => setEnter(true)}
                >
                    <div className={classNames([
                        styles.frameForDropping,
                        { [styles.active]: enter }
                    ])}>
                        <div className={styles.dropCloudIcon}>
                            <DropCloud />
                        </div>

                        <div className={styles.description}>
                            Перетащите файл или{" "}
                            <span
                                onClick={onTargetClick}
                                className={styles.openDialogWindowLabel}
                            >откройте</span>
                        </div>

                        <div className={styles.mobileDescription}>
                            <span
                                onClick={onTargetClick}
                                className={styles.mobileOpenDialogWindowLabel}
                            >Загрузить</span>
                        </div>

                        <div className={styles.formats}>
                            Файл: JPG, PNG, PDF
                        </div>
                    </div>
                </FileDrop>

                <input
                    onChange={onFileInputChange}
                    ref={fileInputRef}
                    type="file"
                    className={styles.hiddenInput}
                    multiple
                />
            </div>

            <div className={styles.uploadedFilesList}>
                {files.length > 0 &&
                    files.map((f, key) =>
                        <div
                            className={styles.uploadedFile}
                            key={key}
                        >
                            <div className={styles.uploadedFileName}>
                                {f.name}
                            </div>
                            <div className={styles.checkMarkHolder}>
                                <CheckMark />
                            </div>
                        </div>
                    )
                }
            </div>
        </>
    )
}

export default DropFiles
