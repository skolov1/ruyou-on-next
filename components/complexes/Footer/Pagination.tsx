import React, {FC, useState, useEffect, useRef, useMemo} from "react";
import classNames from 'classnames'
import styles from './Footer.module.scss'
import FooterActiveItem from "./FooterActiveItem";

type Props = {
    slidesAmount: number,
    currentSlide: number,
    onChange: (slide: number) => void
}

const Pagination: FC<Props> = ({
    slidesAmount,
    currentSlide,
    onChange,
}) => {
    const paginationRef = useRef<HTMLDivElement>(null)
    const toggleRef = useRef<HTMLUListElement>(null)

    const [width, setWidth] = useState(0)
    const [height, setHeight] = useState(0)
    const [togglerWidth, setTogglerWidth] = useState(0)

    useEffect(() => {
        const calcRect = () => {
            if (!paginationRef.current || !toggleRef.current) {
                return
            }

            const rect = paginationRef.current.getBoundingClientRect()
            setHeight(rect.height)
            setWidth(rect.width)

            const togglerRect = toggleRef.current.getBoundingClientRect()
            setTogglerWidth(togglerRect.width)
        }

        calcRect()

        window.addEventListener('resize', calcRect)

        return () => {
            window.removeEventListener('resize', calcRect)
        }
    }, [slidesAmount])


    const left = useMemo(() => {
        if (togglerWidth < width) {
            return (width - togglerWidth) / 2
        }

        const itemWidth = togglerWidth / slidesAmount;


        let left = (width / 2 - itemWidth / 2) - (itemWidth * currentSlide - itemWidth)

        if (left > 0) {
            return 0
        }

        if (left < (togglerWidth - width) * -1) {
            return (togglerWidth - width) * -1
        }

        return left
    }, [width, togglerWidth, slidesAmount, currentSlide])

    console.log('Pagination', {left, width, togglerWidth, slidesAmount, currentSlide, 'width - togglerWidth': width - togglerWidth})


    return (
        <div className={styles.pagination} ref={paginationRef}>
            <div className={classNames(styles.paginationLeft, {
                [styles.show]: left < 0
            })}/>
            <ul
                className={styles.toggler}
                ref={toggleRef}
                style={{height, left}}
            >
                {Array(slidesAmount).fill(1).map((_, i) => {
                    const num = i + 1

                    return (
                        <li
                            key={num}
                            onClick={() => onChange(num)}
                            className={classNames(
                                styles.togglerItem,
                                { [styles.active]: num === currentSlide }
                            )}
                        >
                            {`0${num}`}
                            {currentSlide === num && <FooterActiveItem />}
                        </li>
                    )
                })}
            </ul>
            <div className={classNames(styles.paginationRight, {
                [styles.show]: left > width - togglerWidth
            })} />
        </div>
    )
}

export default Pagination