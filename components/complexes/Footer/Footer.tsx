import styles from './Footer.module.scss'
import { FC, useCallback } from 'react'
import classNames from 'classnames'
import { AnimateSharedLayout } from 'framer-motion'
import FooterActiveItem from './FooterActiveItem'
import Pagination from "./Pagination";

type Props = {
  slidesAmount?: number,
  currentSlide?: number,
  minimal?: boolean
  onChange?: (slide: number) => void
}

const Footer: FC<Props> = ({
  slidesAmount,
  currentSlide,
  minimal,
  onChange
}) => {

  const onHandleChangeSlide = useCallback((slide: number) => {
    if (onChange) {
      onChange(slide)
    }
  }, [onChange])

  const getItems = useCallback(() => {
    let arr: JSX.Element[] = []
    for (let i = 1; i <= slidesAmount; i++) {
      arr.push(
        <li
          key={i}
          onClick={() => onHandleChangeSlide(i)}
          className={classNames(
            styles.togglerItem,
            { [styles.active]: i === currentSlide }
          )}
        >
          {`0${i}`}
          {currentSlide === i && <FooterActiveItem />}
        </li>
      )
    }
    return arr
  }, [slidesAmount, currentSlide])

  return (
    <AnimateSharedLayout>
    <footer className={styles.footer} style={{ height: '80px' }}>
        <div className={styles.footerSide}>
          {/*<div className={styles.langToggle}>{minimal ? <a href='mailto:support@ruyou.ru'>support@ruyou.ru</a> : 'eng'}</div>*/}
        </div>
        <div className={styles.paginationWrap}>
          {
            slidesAmount !== undefined &&
            currentSlide !== undefined &&
            !!onChange && (
                <Pagination
                    slidesAmount={slidesAmount}
                    currentSlide={currentSlide}
                    onChange={onChange}
                />
            )
          }
        </div>
        <div className={styles.footerSide}>
          <div className={styles.copyRight}>© 2012-2021 RUYOU</div>
        </div>
    </footer>
    </AnimateSharedLayout>
  )
}

export default Footer