import styles from './Footer.module.scss'
import { FC } from 'react'
import { motion } from 'framer-motion'

type Props = {
}

const FooterActiveItem: FC<Props> = ({ }) =>  {
    return (
      <motion.div
        layoutId="activeItem"
        className={styles.footerActiveItem}
      />
    );
  }

export default FooterActiveItem