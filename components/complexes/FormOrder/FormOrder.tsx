import React, { FC, ReactNode, useCallback, useState, useMemo, ChangeEvent } from 'react'
import classNames from 'classnames'
import {validateEmail as validateEmailLib, validatePhone as validatePhoneLib} from "../../../utils/string";
import { OptionsItem, Select } from '../../simples/Select'
import { TextField } from '../../simples/TextField'
import { MaskTextField } from '../../simples/MaskTextField'
import { DropFiles } from '../DropFiles'
import { CheckBox } from '../../simples/CheckBox'
import {formOrderUrl} from "../../../constants";
import styles from './FormOrder.module.scss'

type Props = {}

const ServicesOptions: OptionsItem[] = [
  { value: 1, label: 'Мобильные приложения' },
  { value: 2, label: 'Web-сервисы' },
  { value: 3, label: 'Дизайн' },
  { value: 4, label: 'eCommerce' }
]

const textareaClasses = {
  input: styles.textareaInput,
}

const validateService = (value: number | undefined) => {
  if (!value) {
    return "Укажите услугу"
  }

  return ''
}

const validateName = (value: string) => {
  if (!value) {
    return "Укажите свое имя"
  }

  return ''
}

const validatePhone = (value: string) => {
  if (!value) {
    return "Укажите телефон"
  } else if (!validatePhoneLib(value)) {
    return "Некорректный телефон"
  }

  return ''
}

const validateEmail = (value: string) => {
  if (!value) {
    return ""
  } else if (!validateEmailLib(value)) {
    return "Некорректная почта"
  }

  return ''
}


const FormOrder: FC<Props> = ({
}) => {

  const [service, setService] = useState(undefined)
  const [errorService, setErrorService] = useState('')
  const [touchService, setTouchService] = useState(false)
  const [name, setName] = useState('')
  const [errorName, setErrorName] = useState('')
  const [touchName, setTouchName] = useState(false)
  const [phone, setPhone] = useState('')
  const [errorPhone, setErrorPhone] = useState('')
  const [touchPhone, setTouchPhone] = useState(false)
  const [mail, setMail] = useState('')
  const [errorMail, setErrorMail] = useState('')
  const [touchMail, setTouchMail] = useState(false)
  const [message, setMessage] = useState('')
  const [files, setFiles] = useState<File[]>([])
  const [agree, setAgree] = useState(true)

  const [success, setSuccess] = useState(false)

  const handleServiceChange = (value: number) => {
    setService(value)
    setErrorService(validateService(value))
  }
  const handleServiceBlur = () => {
    setTouchService(true)
    setErrorService(validateService(service))
  }

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    setName(value)
    setErrorName(validateName(value))
  }
  const handleNameBlur = () => {
    setTouchName(true)
    setErrorName(validateName(name))
  }

  const handlePhoneChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    setPhone(value)
    setErrorPhone(validatePhone(value))
  }
  const handlePhoneBlur = () => {
    setTouchPhone(true)
    setErrorPhone(validatePhone(phone))
  }

  const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    setMail(value)
    setErrorMail(validateEmail(value))
  }
  const handleEmailBlur = () => {
    setTouchMail(true)
    setErrorMail(validateEmail(mail))
  }

  const [submiting, setSubmiting] = useState(false)
  const submit = () => {

    setTouchService(true)
    setTouchName(true)
    setTouchPhone(true)
    setTouchMail(true)

    const checkServiceError = validateService(service)
    const checkNameError = validateName(name)
    const checkPhoneError = validatePhone(phone)
    const checkEmailError = validateEmail(mail)

    if (checkServiceError || checkNameError || checkPhoneError || checkEmailError) {
      setErrorService(checkServiceError)
      setErrorName(checkNameError)
      setErrorPhone(checkPhoneError)
      setErrorMail(checkEmailError)
      return
    }


    setSubmiting(true)



    const form  = new FormData()

    const findService = ServicesOptions.find(s => s.value === service)
    if (!findService) {
      console.error('Service not found', {service, findService})
      return
    }
    form.append('service', findService.label as 'string')
    form.append('name', name)
    form.append('phone', phone)
    form.append('email', mail)
    form.append('message', message)

    files.forEach(file => {
      form.append('files[]', file)
    })

    fetch(formOrderUrl, {
      method: 'POST',
      body: form
    }).then(response => {
      return response.json()
    }).then(() => {
      setSubmiting(false)
      setSuccess(true)

      setTimeout(() => {
        setSuccess(false)
      }, 10000)
    })
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.mainCentering}>

        <div className={styles.formTitle}>Узнайте стоимость разработки проекта</div>

        <div className={styles.holderParagraph}>
          <p className={classNames([
            styles.formParagraph,
            styles.formMail
          ])}>support@ruyou.ru</p>
          <p className={styles.formParagraph}>+7 (499) 444-13-27</p>
        </div>


        <div className={styles.row}>
          <div className={styles.col}>
            <Select
              label="Выберите услугу *"
              options={ServicesOptions}
              onChange={handleServiceChange}
              onBlur={handleServiceBlur}
              value={service}
              helperText={touchService && errorService ? errorService : ''}
              error={touchService && !!errorService}
            />
          </div>
          <div className={styles.col}>
            <TextField
              label='Ваше имя *'
              value={name}
              onChange={handleNameChange}
              onBlur={handleNameBlur}
              helperText={touchName && errorName ? errorName : ''}
              error={touchName && !!errorName}
            />
          </div>
        </div>


        <div className={styles.row}>
          <div className={styles.col}>
            <MaskTextField
              label='Ваш номер *'
              value={phone}
              id='my-input-id'
              name="phone"
              mask="+7 (999) 999 99 99"
              onChange={handlePhoneChange}
              onBlur={handlePhoneBlur}
              helperText={touchPhone && errorPhone ? errorPhone : ''}
              error={touchPhone && !!errorPhone}
            />
          </div>
          <div className={styles.col}>
            <TextField
              label='Ваша почта'
              value={mail}
              onChange={handleEmailChange}
              onBlur={handleEmailBlur}
              helperText={touchMail && errorMail ? errorMail : ''}
              error={touchMail && !!errorMail}
            />
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <TextField
              label='Ваше сообщение'
              value={message}
              onChange={(e) => setMessage(e.target.value)}
              classes={textareaClasses}
              multiline
            />
          </div>
        </div>

        <div className={styles.dropFilesHolder}>
          <DropFiles
            onChange={setFiles}
            files={files}
          />
        </div>

        <div className={styles.acceptTerms}>
          <div className={styles.checkBoxHolder}>
            <CheckBox
              checked={agree}
              onChange={setAgree}
            />
          </div>
          <p
            className={styles.acceptTermsLabel}
            onClick={() => setAgree(!agree)}
          >
            Я принимаю условия {" "}
            <a className={styles.linkToTerm} href="#">
              Политики обработки персональных данных
            </a>{" "}
            и даю свое согласие на
            обработку моих персональных данных.
          </p>
        </div>

        <div className={styles.buttons}>
          <div
              className={classNames([
                styles.sendButton,
                {
                  [styles.disabled]: !agree,
                  [styles.loading]: submiting
                }
              ])}
              onClick={submit}
          >
            <span className={styles.sendButtonContain}>Отправить</span>
            <span className={styles.sendButtonLoader}>
                <span />
                <span />
                <span />
            </span>

          </div>

          {success && (
              <div className={styles.success}>Спасибо за заявку. 🎉😃👍<br /> Мы свяжемся с вами в ближайшее время! 😘</div>
          )}

        </div>


      </div>
    </div>
  )
}

export default FormOrder
