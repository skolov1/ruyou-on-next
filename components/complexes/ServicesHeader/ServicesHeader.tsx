import styles from './ServicesHeader.module.scss'
import {FC, useCallback, useContext, useMemo} from 'react'
import { Header } from '../Header'
import { SideSocial } from '../SideSocial'
import Link from 'next/link'
import classnames from 'classnames'
import { motion } from 'framer-motion'
import { useRouter } from 'next/router'
import {StylesContext} from "../../../pages/_app";
import Cube from "../../../public/cube-black.svg";
import Sphere from "../../../public/sphere-black.svg";
import Cone from "../../../public/cone-black.svg";
import Cylinder from "../../../public/cylinder-black.svg";

const Services = [
  'Мобильные приложения',
  'Web-сервисы',
  'Дизайн',
  'eCommerce'
]

type Props = {
  currentServices: number
  hugeTitle: string
  actionWordsCount: number
}

const ServicesHeader: FC<Props> = ({
  currentServices,
  hugeTitle,
  actionWordsCount,
}) => {

  const { servicesPageStyle } = useContext(StylesContext)

  const router = useRouter()


  const prepareSlide = useCallback(() => {
    let actionWords: string = ''
    let stringWords: string[] = hugeTitle.split(' ')
    for (var i = 0; i<actionWordsCount; i++) {
        actionWords += ' ' + stringWords.shift()
    }
    return {
      actionWords: actionWords,
      otherText: stringWords.join(' ')
    }
  }, [])

  let { actionWords, otherText } = prepareSlide()

  const targetHeight = useMemo(() => {
    if (!process.browser) {
      return 680
    }
    const clientHeight = document.documentElement.clientHeight
    let targetHeight = clientHeight * 0.8
    if (targetHeight > 680) {
      targetHeight = 680
    } else if (targetHeight < 180) {
      targetHeight = 180
    }

    return targetHeight
  }, [])


  return (
    <motion.div
      className={styles.wrapper}
      animate={{ height: targetHeight }}
      transition={{
        duration: 1
      }}
    >

      <div className={styles.figure}>
        {currentServices === 0 && <Cube />}
        {currentServices === 1 && <Sphere />}
        {currentServices === 2 && <Cone />}
        {currentServices === 3 && <Cylinder />}
        {currentServices === 4 && <Cube />}
        {currentServices === 5 && <Sphere />}
      </div>

      <Header />

      <div className={styles.headHolder}>

        <SideSocial />

        <div className={styles.centerInfo}>
          <div className={styles.hugeTitle}>
            <span>{actionWords}</span> {' '}
            {otherText}
          </div>
        </div>

        <div className={styles.sideSwitcher}>
          <ul className={styles.sideSwitcherList}>
            {Services.map((s, i) =>
              <Link href={`/services/${i + 1}`} key={i}>
                <li className={classnames([
                  styles.sideSwitcherListItem,
                  { [styles.active]: i === currentServices }
                ])}>
                  {s}
                </li>
              </Link>
            )}
          </ul>
        </div>

        <Link href="/">
          <div className={styles.toMainButton}>
            На главную
          </div>
        </Link>



      </div>
    </motion.div>
  )
}

export default ServicesHeader
