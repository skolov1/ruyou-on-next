import styles from '../../../pages/projects/Projects.module.scss'
import { FC, useCallback, useEffect, useRef, useState, } from 'react'
import Slider from 'react-slick'
import PixliIcon from '../../../public/projects/logo/pixli-logo-icon.svg'
import BigamIcon from '../../../public/projects/logo/bigam-logo-icon.svg'
import SuencoIcon from '../../../public/projects/logo/suenco-logo-icon.svg'
import MetroIcon from '../../../public/projects/logo/metro-logo-icon.svg'
import SmallIcon from '../../../public/projects/logo/small-logo-icon.svg'
import Link from 'next/link'

import BadenLogo from '../../../pages/projects/logos/baden-logo-tape.svg'
import EyelashLogo from '../../../pages/projects/logos/eyelash-logo-tape.svg'
import NovokurLogo from '../../../pages/projects/logos/novokur-logo-tape.svg'
import PivkoLogo from '../../../pages/projects/logos/pivko-logo-tape.svg'
import TeleplusLogo from '../../../pages/projects/logos/teleplus-logo-tape.svg'
import ZartexLogo from '../../../pages/projects/logos/zartex-logo-tape.svg'


type Props = {
    onChangeSlide: (current: number, next: number) => void
    onInitSlide: (slidesNumber: number) => void
    setCurrentNumberSlide: number
    refSlide: any
}

const SliderItem: FC<Props> = ({
    onChangeSlide,
    onInitSlide,
    setCurrentNumberSlide,
    refSlide
}) => {

    const [slideIndex,setSlideIndex] = useState(0)

    const [width, setWidth] = useState(undefined)

    const setting = {
        centerMode: true,
        centerPadding: "0px",
        className: 'sliderProjects', // unique styles for the slider are rendered into a globals.scss
        slidesToShow: 3,
        speed: 500,
        arrows: false,
        infinite: false,
        beforeChange: (current, next) => {
            onChangeSlide(current, next + 1)
            setSlideIndex(next)
        },
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 1
                }
            }
        ]
    }


    const onResizeWindow = useCallback(() => {
        setWidth(window.innerWidth)
    }, [])

    useEffect(() => {
        if (refSlide && refSlide.current) {
            onInitSlide(refSlide!.current!.props!.children.length - 2)
        }
    }, [refSlide, refSlide.current])

    useEffect(() => {
        if (
            setCurrentNumberSlide &&
            refSlide &&
            refSlide.current &&
            refSlide.current.slickGoTo
        ) {
            refSlide!.current!.slickGoTo(setCurrentNumberSlide - 1)
        }
    }, [setCurrentNumberSlide])


    useEffect(() => {
        if (window) {
            setWidth(window.innerWidth)
        }
        window.addEventListener('resize', onResizeWindow)
        return () => {
            window.removeEventListener('resize', onResizeWindow)
        }
    }, [])


    return refSlide ?
        <Slider {...setting} ref={refSlide}>

            <Link href="/projects/4">
                <div className={slideIndex >= 1 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.slideInfoHolder}>
                            {/*2020*/}
                            <div className={styles.slideProjName}>Small</div>
                        </div>
                    </div>
                    <img src='/projects/small/small-slider.png' />
                </div>
            </Link>

            <Link href="/projects/2">
                <div className={slideIndex >= 2 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.slideInfoHolder} style={{fontFamily:'Gotham Pro', fontWeight:500}}>
                            {/*Готовится к релизу 2021*/}
                            <div className={styles.slideProjName}>Премьер Зал</div>
                        </div>
                    </div>
                    <img src='/projects/premier/premier-slider.png' />
                </div>
            </Link>

            <Link href="/projects/3">
                <div className={slideIndex >= 3 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.logoHolder}>
                            <BigamIcon className={styles.logoHolder} />
                        </div>
                        <div className={styles.slideInfoHolder}>
                            {/*2020*/}
                            <div className={styles.slideProjName}>Bigam</div>
                        </div>
                    </div>
                    <img src='/projects/bigam/bigam-slide.png' />
                </div>
            </Link>

            <Link href="/projects/1">
                <div className={slideIndex >= 4 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.slideInfoHolder} style={{fontFamily:'Gotham Pro', fontWeight:500}}>
                            {/*Готовится к релизу 2021*/}
                            <div className={styles.slideProjName}>G-Group</div>
                        </div>
                    </div>
                    <img src='/projects/g-group/g-group-slider.png' />
                </div>
            </Link>

            <Link href="/projects/5">
                <div className={slideIndex >= 5 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.slideInfoHolder}>
                            {/*2020*/}
                            <div className={styles.slideProjName}>Я на отдых</div>
                        </div>
                    </div>
                    <img src='/projects/on-relax/on-relax-slider.png' />
                </div>
            </Link>

            <Link href="/projects/6">
                <div className={slideIndex >= 6 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.logoHolder}>
                            <SuencoIcon className={styles.logoHolder} />
                        </div>
                        <div className={styles.slideInfoHolder}>
                            {/*2021*/}
                            <div className={styles.slideProjName}>СУЭНКО</div>
                        </div>
                    </div>
                    <img src='/projects/suenco/suenco-slider.png' />
                </div>
            </Link>

            <Link href="/projects/7">
                <div className={slideIndex >= 7 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.logoHolder}>
                            <PixliIcon className={styles.logoHolder} />
                        </div>
                        <div className={styles.slideInfoHolder}>
                            {/*2020*/}
                            <div className={styles.slideProjName}>PIXLI</div>
                        </div>
                    </div>
                    <img src='/projects/pixli/pixli-slide.png' />
                </div>
            </Link>

            <Link href="/projects/8">
                <div className={slideIndex >= 8 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder}>
                        <div className={styles.slideInfoHolder}>
                            {/*2020*/}
                            <div className={styles.slideProjName}>Metro</div>
                        </div>
                    </div>
                    <img src='/projects/metro/metro-slider.png' />
                </div>
            </Link>

            <Link href="/projects/9">
                <div className={slideIndex >= 9 ? styles.slideItemPrev : styles.slideItem}>
                    <div className={styles.slideIconHolder} style={{flexDirection:'column'}}>
                        <div className={styles.logoHolder}>
                        </div>
                        {/*<div className={styles.slideTitle}>*/}
                        {/*    Урал*/}
                        {/*</div>*/}
                        <div className={styles.slideInfo}>
                            Другие проекты
                        </div>
                        <div className={styles.bottomLogos}>
                            <NovokurLogo className={styles.bottomLogo} style={{width:'99px'}}/>
                            <BadenLogo className={styles.bottomLogo} style={{width:'43px'}}/>
                            <TeleplusLogo className={styles.bottomLogo} style={{width:'95px'}}/>
                            <ZartexLogo className={styles.bottomLogo} style={{width:'85px'}}/>
                            <EyelashLogo className={styles.bottomLogo} style={{width:'87px'}}/>
                            {/*<PivkoLogo className={styles.bottomLogo} style={{width:'92px'}}/>*/}
                        </div>
                    </div>
                    <img src='/projects/common/proj-slide.png' />
                </div>
            </Link>

            {width > 768 && <div className={styles.slideItem} />}
            {width > 768 && <div className={styles.slideItem} />}
        </Slider> : null
}

export default SliderItem

