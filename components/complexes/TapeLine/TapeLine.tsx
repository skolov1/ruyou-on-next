import styles from './TapeLine.module.scss'
import { FC, } from 'react'
import Slider from 'react-slick'

type Props = {}

const TapeLine: FC<Props> = ({
}) => {

  var setting = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    autoplay: true,
    speed: 4000,
    autoplaySpeed: 4000,
    cssEase: "linear",
    swipe: false,
    waitForAnimate: false,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1
        }
      },

      {
        breakpoint: 1024,
        settings: {
          slidesToScroll: 3,
          slidesToShow: 3
        }
      },

      {
        breakpoint: 1420,
        settings: {
          slidesToScroll: 5,
          slidesToShow: 5
        }
      },

    ]
  }

  return (
    <Slider {...setting}>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/baden_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/bigam_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/goverment_kal_ico.png' />
          <div className={styles.textSlide}>Правительство Калининградской области</div>
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/goverment_yamal_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/merlen_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/metro_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/novocur_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/on_relax_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/press_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/shaker_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/small_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/smartfeedback_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/spoon_fork_ico.png' />
        </div>
      </div>

      <div className={styles.slide}>
        <div className={styles.center}>
          <img src='/projects/teleplus_ico.png' />
        </div>
      </div>
    </Slider>
  )
}

export default TapeLine
