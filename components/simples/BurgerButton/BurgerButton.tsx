import styles from './BurgerButton.module.scss'
import React, { FC, useCallback, useState } from 'react'
import classNames from 'classnames'
import { siteSlice } from '../../../store/slices/site'
import { useAppDispatch, useAppSelector } from '../../hooks/store'
import { selectSideMenuStatus } from '../../../store/slices/site'

type Props = {}

const BurgerButton: FC<Props> = ({ }) => {

  const { changeMenuStatus } = siteSlice.actions

  const dispatch = useAppDispatch()
  const select = useAppSelector(selectSideMenuStatus)

  const onHandleOpen = useCallback(() => {
    dispatch(changeMenuStatus(true))
  }, [])

  const onHandleClose = useCallback(() => {
    dispatch(changeMenuStatus(false))
  }, [])

  return (
    <div
      className={classNames(styles.wrapper, { [styles.opened]: select })}
      onClick={select ? onHandleClose : onHandleOpen}
    >
      <span />
      <span />
    </div>
  )
}

export default BurgerButton
