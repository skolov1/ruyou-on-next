import React, {
    FC,
    InputHTMLAttributes,
    useCallback,
    useEffect,
    useState,
    FocusEvent,
    forwardRef,
} from 'react'
import _ from 'lodash'
import classNames from 'classnames'
import style from './MaskTextField.module.scss'

import InputMask, { Props as InputMaskProps } from 'react-input-mask'

export type Props = InputMaskProps & {
    id?: string
    label?: string
    helperText?: string
    error?: boolean
    classes?: {
        root?: string
        label?: string
        input?: string
    }
    outLabel?: boolean
    showRequired?: boolean
}

/**
 * Компонент Input
 * @param {string|undefined} id - идентификатор
 * @param {string} label - Название текстового поля
 * @param value - Значение текстового поля
 * @param {string} helperText - Текст подсказки под текстовым полем
 * @param {boolean} error - Подсветка ошибки
 * @param {boolean} multiline - Если true - выводиться textarea
 * @param classes - Дополнительные стилевые классы
 * @param onChange - Callback изменения текстового поля
 * @param onFocus - Callback фокуса
 * @param onBlur - Callback потери фокуса
 * @param rest
 * @constructor
 */

// Пример формирования маски из нескольких параметров

// const firstLetter = /(?!.*[DFIOQU])[A-VXY]/i;
// const letter = /(?!.*[DFIOQU])[A-Z]/i;
// const digit = /[0-9]/;
// const mask = [firstLetter, digit, letter, " ", digit, letter, digit];
// 9	0-9         - маска для вывода цифр
// a	a-z, A-Z    - маска для вывода букв
// *	0-9, a-z, A-Z  - маска для вывода букв и цифр

const MaskTextField = forwardRef<HTMLInputElement, Props>(
    (
        {
            id: iid,
            label,
            value,
            helperText,
            error = false,
            classes = {},
            onChange,
            onFocus,
            onBlur,
            disabled,
            outLabel = false,
            showRequired = false,
            ...rest
        },
        ref
    ) => {
        const [id, setId] = useState('')
        const [focus, setFocus] = useState(false)

        // Получение идентификатора
        // Если идентификатор отсутствует, генерируется случайная строка
        useEffect(() => {
            const id = iid || _.uniqueId('text-field')
            setId(id)
        }, [iid, setId])

        // Обработчик фокуса
        // При фокусировки добавляется класс, который меняет местоположение label
        const onHandleFocus = useCallback(
            (event: FocusEvent<HTMLInputElement>) => {
                setFocus(true)
                if (onFocus) {
                    onFocus(event)
                }
            },
            [setFocus, onFocus]
        )

        // Обработка потери фокуса
        // Удаляется класс, который устанавливался при фокусировке
        const onHandleBlur = useCallback(
            (event: FocusEvent<HTMLInputElement>) => {
                setFocus(false)
                if (onBlur) {
                    onBlur(event)
                }
            },
            [onBlur]
        )

        // Вывод компонента
        return (
            <div
                className={classNames(style.root, classes.root, {
                    [style.focus]: focus,
                    [style.filled]: !!value,
                    [style.error]: error,
                    [style.disabled]: !!disabled,
                })}
            >
                {outLabel && label && (
                    <div className={style.outLabel}>
                        {label}
                        {showRequired && (
                            <>
                                {' '}
                                <span className={style.requiredIndicator}>
                                    *
                                </span>
                            </>
                        )}
                    </div>
                )}

                <div className={style.base}>
                    {label && !outLabel && (
                        <label
                            className={classNames(style.label, classes.label)}
                            htmlFor={id}
                        >
                            {label}
                            {showRequired && (
                                <>
                                    {' '}
                                    <span className={style.requiredIndicator}>
                                        *
                                    </span>
                                </>
                            )}
                        </label>
                    )}

                    <InputMask
                        {...rest}
                        className={classNames(style.input, classes.input)}
                        id={id}
                        value={value || ''}
                        onFocus={onHandleFocus}
                        onBlur={onHandleBlur}
                        onChange={onChange}
                        disabled={disabled}
                        inputRef={ref}
                    />

                    {label && !outLabel && (
                        <div className={style.notch}>
                            <span className={style.notchContainer}>
                                {label}
                            </span>
                        </div>
                    )}
                </div>

                {helperText && <p className={style.helperText}>{helperText}</p>}
            </div>
        )
    }
)

export default MaskTextField
