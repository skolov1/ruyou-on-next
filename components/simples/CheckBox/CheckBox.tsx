import React, { FC, useCallback, ChangeEvent, InputHTMLAttributes } from 'react'
import classNames from 'classnames'
import style from './CheckBox.module.scss'

type Props = {
    checked?: boolean
    disabled?: boolean
    incomplete?: boolean
    onChange?: (checked: boolean) => void
}

const Checkbox: FC<Props> = ({
    checked = false,
    disabled = false,
    onChange,
    incomplete = false,
}) => {
    const onHandleChange = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            if (onChange) {
                onChange(event.target.checked)
            }
        },
        [onChange]
    )

    return (
        <div
            className={classNames(style.checkbox, {
                [style.checked]: checked,
                [style.disabled]: disabled,
            })}
        >
            <input
                checked={checked}
                type="checkbox"
                className={style.input}
                onChange={onHandleChange}
                disabled={disabled}
            />
            {!!checked && disabled && <div className={style.markDisabled} />}
            {!!checked && incomplete && <div className={style.incomplete} />}
            {!!checked && <MarkIcon />}
        </div>
    )
}

export default Checkbox


const MarkIcon = () => <div className={style.markIcon} />