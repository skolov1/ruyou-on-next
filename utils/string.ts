export const validateEmail = (email: string) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g
    return re.test(String(email).toLowerCase())
}

export const validatePhone = (phone: string) => {
    const re = /^\+7 \(\d\d\d\) \d\d\d \d\d \d\d$/
    return re.test(String(phone).toLowerCase())
}