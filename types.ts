export type Slides = {
    title: string,
    link: string
}

export type Files = {
    name: string
    type: string
    size: number
}

export type Order = {
    youAre: number,
    doing: number,
    name: string,
    phone: string,
    mail: string
    service: number,
    acceptedFiles: File[],
}