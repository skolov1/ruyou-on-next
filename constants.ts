export const emailRE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
export const numberRE = /[0-9]/

export const backendDomain = 'https://back.ruyou.ru/'
export const formOrderUrl = backendDomain + 'site/send-form'
export const orderStepsContainerUrl = backendDomain + 'site/send-form'